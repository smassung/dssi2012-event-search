<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Judgment for classification results</title>
</head>
<body>
<% 
	String user = request.getParameter("user");
	String nid = request.getParameter("nid");
	String judgment = request.getParameter("judgment");
	
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;

	String url = "jdbc:mysql://engr-dssi01.engr.illinois.edu:3306/entitysearch";
	String dbuser = "entitysearch";
	String password = "hello_dssi";	
	
	try {
		con = DriverManager.getConnection(url, dbuser, password);
		st = con.createStatement();
		if(nid!=null){
			st.executeUpdate("update news_judgment set judgment="+judgment+" where id="+nid);
			judgment=null;
		}
	} catch (SQLException ex) {

	} finally {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException ex) {
		}
	}
	
	String text=null;
	String subtopic=null;
	int a=0;
	int b=0;
	int amount=58;
	
	if(user.equals("guo")){
		a=0;
		b=amount+1;
	}else if(user.equals("liao")){
		a=amount;
		b=amount*2+1;
	}else if(user.equals("wang")){
		a=amount*2;
		b=amount*3+1;
	}else if(user.equals("shiralkar")){
		a=amount*3;
		b=amount*4+1;
	}else if(user.equals("naveed")){
		a=amount*4;
		b=amount*5+1;
	}else if(user.equals("bayzick")){
		a=amount*5;
		b=amount*6+1;
	}
	
	

	try {
		con = DriverManager.getConnection(url, dbuser, password);
		st = con.createStatement();
	//	rs = st.executeQuery("select news_judgment.id, text, subtopic from news,news_judgment where news.id=news_judgment.id and news_judgment.id>"
	//			+ a + " and news_judgment.id<" + b + " and judgment=null");
		rs=st.executeQuery("select news_judgment.id as nid, text, news_judgment.subtopic from news, news_judgment where news_judgment.news_id=news.id and news_judgment.id>"
				+a+" and news_judgment.id<" + b + " and judgment is null");

		if (rs.next()) {
			text = rs.getString("text");
			subtopic=rs.getString("subtopic");
			nid = Integer.toString(rs.getInt("nid"));
		}else{
			text=null;
		}

	} catch (SQLException ex) {

	} finally {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}

		} catch (SQLException ex) {
		}
	}		
%>
<%
if(text!=null){
	String[] paras=text.split("\n");
	for(int i=0;i<paras.length;i++){
%>
	<p><%=paras[i] %></p>
<%	
	}
%>
<br><h3>This news is classified as <font color='red'><%=subtopic.toUpperCase() %></font></h3>
<form method="post" action="Judgment.jsp">
	<input type="radio" name="judgment" value="true">Correctly Classified<br>
	<input type="radio" name="judgment" value="false">Incorrectly Classified<br><br>
	<input id="user" type="hidden" name="user" value='<%=user%>'/>
	<input id="nid" type="hidden" name="nid" value='<%=nid%>'/>
	<input type="submit" value="Submit" />
</form>
	<% 
}else{
	%>
	<p>Your part is done. Thanks!</p>
	<%
}
	%>


</body>
</html>