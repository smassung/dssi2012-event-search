package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import common.*;
import java.util.*;
import java.util.logging.*;
import java.sql.*;

public final class Paragraphs_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Paragraphs</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t");
	
	Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    
	String url = "jdbc:mysql://engr-dssi01.engr.illinois.edu:3306/entitysearch";
	String user = "entitysearch";
	String password = "hello_dssi";
	ArrayList<String> text = new ArrayList<String>();

try {
	con = DriverManager.getConnection(url, user, password);
	st = con.createStatement();
	rs = st.executeQuery("SELECT content from news_training_paras;");

	while (rs.next()) {
		text.add(rs.getString("content"));
	}
	
	}catch (SQLException ex) {

    } finally {
        try {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (con != null) {
                con.close();
            }

        } catch (SQLException ex) {
        }
    }
		int currentPage = 1;
        if (request.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(request.getParameter("currentPage"));
        }
        int countPerPage = 10;
		Pagination<String> pagination = new Pagination<String>(currentPage, countPerPage);
        pagination.setRecordSum(text.size());

        List<String> recordList = new ArrayList<String>();
		for (int i=pagination.getPosition()-1;i<(pagination.getPosition()-1+countPerPage>pagination.getRecordSum()?pagination.getRecordSum():pagination.getPosition()-1+countPerPage);i++){
			recordList.add(text.get(i));
		}
        pagination.setRecordList(recordList);
		List<String> array = pagination.getRecordList();
    
      out.write("\r\n");
      out.write("    ");

	for (int i = 0; i < array.size(); i++) {		 		 	
	
      out.write("\r\n");
      out.write("\t<p>");
      out.print(array.get(i));
      out.write("<br/><br/></p>\r\n");
      out.write("\t\t");

		}
	
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("\t");

	        List<Integer> pagesList = pagination.getPages();
	        if (pagesList.size() > 1) {
	            if (pagination.getCurrentPage() != 1) {
	
      out.write("\r\n");
      out.write("\t                <a href='Paragraphs.jsp?currentPage=");
      out.print(pagination.getCurrentPage() - 1);
      out.write("'>Prev</a>\r\n");
      out.write("\t");
}
	            for (int p : pagesList) {
	                if (pagination.getCurrentPage() == p) {
	 
      out.write("\r\n");
      out.write("\t                    <a class=\"p_num p_curpage\">");
      out.print(p);
      out.write("</a>\r\n");
      out.write("\t                ");
} else {
      out.write("\r\n");
      out.write("\t                    <a href='Paragraphs.jsp?currentPage=");
      out.print(p);
      out.write('\'');
      out.write('>');
      out.print(p);
      out.write("</a>\r\n");
      out.write("\t                ");
}
	             }
	            if ((pagination.getTotalPage() > 11) && (pagesList.get(pagesList.size() - 1) != pagination.getTotalPage())) {
      out.write("\r\n");
      out.write("\t                <a >...</a>\r\n");
      out.write("\t            ");
}
	            if (pagination.getCurrentPage() != pagination.getTotalPage()) {
      out.write("\r\n");
      out.write("\t            <a href='Paragraphs.jsp?currentPage=");
      out.print(pagination.getCurrentPage() + 1);
      out.write("'>Next</a>\r\n");
      out.write("\t            ");
}
      out.write("\r\n");
      out.write("\t         ");
}
      out.write("\r\n");
      out.write("    \r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
