<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="common.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.logging.*"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Paragraphs</title>
</head>
<body>
	<%	
	Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    
	String url = "jdbc:mysql://engr-dssi01.engr.illinois.edu:3306/entitysearch";
	String user = "entitysearch";
	String password = "hello_dssi";
	ArrayList<String> text = new ArrayList<String>();

try {
	con = DriverManager.getConnection(url, user, password);
	st = con.createStatement();
	rs = st.executeQuery("SELECT content from news_training_paras;");

	while (rs.next()) {
		text.add(rs.getString("content"));
	}
	
	}catch (SQLException ex) {

    } finally {
        try {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (con != null) {
                con.close();
            }

        } catch (SQLException ex) {
        }
    }
		int currentPage = 1;
        if (request.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(request.getParameter("currentPage"));
        }
        int countPerPage = 10;
		Pagination<String> pagination = new Pagination<String>(currentPage, countPerPage);
        pagination.setRecordSum(text.size());

        List<String> recordList = new ArrayList<String>();
		for (int i=pagination.getPosition()-1;i<(pagination.getPosition()-1+countPerPage>pagination.getRecordSum()?pagination.getRecordSum():pagination.getPosition()-1+countPerPage);i++){
			recordList.add(text.get(i));
		}
        pagination.setRecordList(recordList);
		List<String> array = pagination.getRecordList();
    %>
    <%
	for (int i = 0; i < array.size(); i++) {		 		 	
	%>
	<p><%=array.get(i)%><br/><br/></p>
		<%
		}
	%>
	
	<%
	        List<Integer> pagesList = pagination.getPages();
	        if (pagesList.size() > 1) {
	            if (pagination.getCurrentPage() != 1) {
	%>
	                <a href='Paragraphs.jsp?currentPage=<%=pagination.getCurrentPage() - 1%>'>Prev</a>
	<%}
	            for (int p : pagesList) {
	                if (pagination.getCurrentPage() == p) {
	 %>
	                    <a class="p_num p_curpage"><%=p%></a>
	                <%} else {%>
	                    <a href='Paragraphs.jsp?currentPage=<%=p%>'><%=p%></a>
	                <%}
	             }
	            if ((pagination.getTotalPage() > 11) && (pagesList.get(pagesList.size() - 1) != pagination.getTotalPage())) {%>
	                <a >...</a>
	            <%}
	            if (pagination.getCurrentPage() != pagination.getTotalPage()) {%>
	            <a href='Paragraphs.jsp?currentPage=<%=pagination.getCurrentPage() + 1%>'>Next</a>
	            <%}%>
	         <%}%>
    
</body>
</html>