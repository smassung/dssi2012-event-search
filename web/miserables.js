// This file contains the weighted network of coappearances of characters in
// Victor Hugo's novel "Les Miserables". Nodes represent characters as indicated
// by the labels, and edges connect any pair of characters that appear in the
// same chapter of the book. The values on the edges are the number of such
// coappearances. The data on coappearances were taken from D. E. Knuth, The
// Stanford GraphBase: A Platform for Combinatorial Computing, Addison-Wesley,
// Reading, MA (1993).
//
// The group labels were transcribed from "Finding and evaluating community
// structure in networks" by M. E. J. Newman and M. Girvan.

var miserables = {
  nodes:[
    {nodeName:"Politics", group:7, color:"#e7cb94"},
    {nodeName:"Sports", group:8, color:"#e7cb94"},
    {nodeName:"G8 Summit", group:7, color:"#b5cf6b"},
    {nodeName:"NATO Summit", group:7, color:"#b5cf6b"},
    {nodeName:"NBA Final", group:8, color:"#9c9ede"},
    {nodeName:"Super Bowl", group:8, color:"#9c9ede"},
    {nodeName:"Traffic", group:9, color:"#d6616b"},
    {nodeName:"Protest", group:9, color:"#d6616b"},
    {nodeName:"Crime", group:9, color:"#d6616b"},
    {nodeName:"Event", group:10, color:"#e7ba52"},
    {nodeName:"Subtopic", group:11, color:"#bd9e39"},
    {nodeName:"Disaster", group:8, color:"#e7cb94"},
    {nodeName:"Wildfire", group:8, color:"#9c9ede"},//12
    {nodeName:"Festival", group:7, color:"#e7cb94"},
    {nodeName:"Bonnaroo", group:7, color:"#e7cb94"}
  
  ],
  links:[
	{source:0, target:2, value:1},
	{source:0, target:3, value:1},
	{source:1, target:4, value:1},
	{source:1, target:5, value:1},
	{source:9, target:0, value:1},
	{source:9, target:1, value:1},
	{source:10, target:6, value:1},
	{source:10, target:7, value:1},
	{source:10, target:8, value:1},
	{source:9, target:11, value:1},
	{source:11, target:12, value:1},
	{source:9, target:13, value:1},
	{source:13, target:14, value:1}
  ]
};