var LinkedList = function()	{
	this._length = 0;
	this._head = null;
}

LinkedList.prototype = {

	
	
	addNode: function (Id,value){

		// This is the node
        var node = {
				Id: Id,
				value:value,
                next: null	// Reference to the next object
				/**
				*	You can add n number of datas here according to your requirement.
				*/
            },

            current;

        if (this._head === null){
            this._head = node;
        } else {
            current = this._head;

            while(current.next){
                current = current.next;
            }

            current.next = node;
        }

        this._length++;
        return node;

    },
	
	getNode: function(Id){

		var current = this._head;	// Get first node
		
		while(current != null && current.Id != Id){	// Traverse until u find your object
			current = current.next;
		}
		if(current != null)
		return current;
		return null;	// No such Id       
    },

    removeNode: function(Id){

		var current = this._head, previous;
	   
			while(current != null && current.Id != Id ){
				previous = current;
				current = current.next;
			}

			if(current != null)	{
				previous.next = current.next;
				this._length--;
				return current;
			}
		return null;
    },
    
    removeAll: function(){
    	this._length = 0;
    	this._head = null;
    },
	
};
