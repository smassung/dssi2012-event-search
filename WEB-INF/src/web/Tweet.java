package web;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

/**
 * ORM-like object for tweet data. 
 * 
 * @author Chase Geigle
 */
public class Tweet {
    private int id;
    private String body;
    private int eventId;
    private String sentiment; 
    private boolean fromDb;

    public Tweet() {
        fromDb = false;
    }

    public Tweet( ResultSet result ) throws SQLException {
        id = result.getInt("id");
        body = new String(result.getString("body"));
        eventId = result.getInt("event_id");
        sentiment = new String(result.getString("sentiment"));
        fromDb = true;
    }

    public Tweet id( int id ) {
        this.id = id;
        return this;
    }

    public int id() {
        return id;
    }

    public Tweet body( String body ) {
        this.body = body;
        return this;
    }

    public String body() {
        return body;
    }

    public Tweet eventId( int id ) {
        eventId = id;
        return this;
    }

    public int eventId() {
        return eventId;
    }

    public Tweet sentiment( String sentiment ) {
        this.sentiment = new String(sentiment);
        return this;
    }

    public String sentiment() {
        return sentiment;
    }

    public boolean save() {
        if( fromDb )
            return update();
        else
            return create();
    }

    private boolean update() {
        try {
            String update = "UPDATE tweets SET body = ?, event_id = ?, sentiment = ? WHERE id = ?";
            PreparedStatement prepStatement = SQLConnection.getConnection().prepareStatement(update);
            prepStatement.setString(1, this.body);
            prepStatement.setInt(2, this.eventId);
            prepStatement.setString(3, this.sentiment);
            prepStatement.setInt(4, this.id);
            int numUpdated = prepStatement.executeUpdate();
            if( numUpdated != 1 ) {
                System.err.println("WARNING: numUpdated == " + numUpdated + "!");
                System.err.println("Query was: ");
                System.err.println("\t" + prepStatement.toString());
                return false;
            } else
                return true;
        } catch( Exception e ) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean create() {
        try {
            String create = "INSERT INTO tweets (body, event_id, sentiment) VALUES (?, ?, ?)";
            PreparedStatement prepStatement = SQLConnection.getConnection().prepareStatement(create);
            prepStatement.setString(1, this.body);
            prepStatement.setInt(2, this.eventId);
            prepStatement.setString(3, this.sentiment);
            if( prepStatement.executeUpdate() != 1 )
                return false;
            return true;
        } catch( Exception e ) {
            e.printStackTrace();
            return false;
        }
    }

    public static Tweet findById( int id ) {
        try {
            String query = "SELECT * FROM tweets WHERE id = ?";
            PreparedStatement prepStatement = SQLConnection.getConnection().prepareStatement(query);
            prepStatement.setInt(1, id);
            ResultSet result = prepStatement.executeQuery();
            // if we got a result
            if( result.next() )
                return new Tweet( result );
        } catch( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Tweet> findByEventId( int id ) {
        ArrayList<Tweet> tweets = new ArrayList<Tweet>();
        try {
            String query = "SELECT * FROM tweets WHERE event_id = ?";
            PreparedStatement prepStatement = SQLConnection.getConnection().prepareStatement(query);
            prepStatement.setInt(1, id);
            ResultSet result = prepStatement.executeQuery();
            while( result.next() )
                tweets.add( new Tweet(result) );
        } catch( Exception e ) {
            e.printStackTrace();
        }
        return tweets;
    }
}
