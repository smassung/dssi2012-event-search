package web;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ORM-like object for news article data.
 * 
 * @author Chase Geigle
 */
public class NewsArticle {
    private int id;
    private String url;
    private String title;
    private String text;
    private Date date;
    private String city;
    private String state;
    private int newspaperId;
    private String query;
    private int videoId;
    private double trafficScore;
    private double crimeScore;
    private double protestScore;

    private boolean fromDb;

    private int eventId;

    private static HashMap<Integer, Float> negativeSentiment = new HashMap<Integer, Float>();
    private static HashMap<Integer, Float> positiveSentiment = new HashMap<Integer, Float>();

    public NewsArticle() {
        fromDb = false;
        eventId = 0;
    }

    public NewsArticle( ResultSet result ) throws SQLException {
        // metadata
        id      = result.getInt("id");
        url     = result.getString("url");
        title   = result.getString("title");
        text    = result.getString("text");
        date    = result.getDate("date");
        city    = result.getString("city");
        state   = result.getString("state");
        newspaperId = result.getInt("newspaper_id");

        // what was used to find this article
        query   = result.getString("query");
        // used for commens
        videoId = result.getInt("video_id");

        // classification scores
        trafficScore = result.getDouble("traffic_score");
        crimeScore   = result.getDouble("crime_score");
        protestScore = result.getDouble("protest_score");
        fromDb  = true;
        eventId = 0;
    }

    public int id() {
        return id;
    }

    public String url() {
        return url;
    }

    public String title() {
        return title;
    }

    public String text() {
        return text;
    }

    public Date date() {
        return date;
    }

    public String city() {
        return city;
    }

    public String state() {
        return state;
    }

    public String location() {
        return city + ", " + state;
    }

    public int newspaperId() {
        return newspaperId;
    }

    public String query() {
        return query;
    }

    public int videoId() {
        return videoId;
    }

    public List<String> subEvents() {
        ArrayList<String> subs = new ArrayList<String>();
        if( trafficScore > 0 )
            subs.add("traffic");
        if( crimeScore > 0 )
            subs.add("crime");
        if( protestScore > 0 )
            subs.add("protest");
        if( subs.size() == 0 )
            subs.add("other");
        return subs;
    }

    // DERP
    public String event() {
        if( query.matches( ".*super bowl.*" ) )
            return "super bowl";
        if( query.matches( ".*g8 summit.*" ) )
            return "g8 summit";
        if( query.matches( ".*bonnaroo.*" ) )
            return "bonnaroo";
        if( query.matches( ".*nba.*" ) )
            return "nba finals";
        if( query.matches( ".*city park.*" ) ) 
            return "city park jazz";
        if( query.matches( ".*nato.*" ) ) 
            return "nato summit";
        if( query.matches( ".*ribfest.*" ) )
            return "ribfest chicago";
        if( query.matches( ".*indy 500.*" ) )
            return "indy 500";
        if( query.matches( ".*bamboozle.*" ) )
            return "bamboozle";
        if( query.matches( ".*sturgis.*" ) )
            return "sturgis";
        if( query.matches( ".*wildfire.*" ) )
            return "wildfire";
        if( query.matches( ".*bumbershoot.*" ) )
            return "bumbershoot";
        return "unknown";
    }

    public int eventId() {
        if( eventId == 0 ) {
            try {
                String query = "SELECT id FROM events WHERE name = ?";
                PreparedStatement prepStatement = SQLConnection.getConnection().prepareStatement(query);
                prepStatement.setString(1, event());
                ResultSet result = prepStatement.executeQuery();
                if( result.next() )
                    eventId = result.getInt("id");
            } catch( Exception e ) {
                e.printStackTrace();
            }
        }
        return eventId;
    }

    public float sentiment() {
        if( !negativeSentiment.containsKey( eventId() ) )
            sentiments();
        return negativeSentiment.get( eventId() );
    }

    public float positiveSentiment() {
        if( !positiveSentiment.containsKey( eventId() ) )
            sentiments();
        return positiveSentiment.get( eventId() );
    }

    private void sentiments() {
        int positive = 0;
        int negative = 0;
        int neutral = 0;
        int eventid = eventId();
        List<Tweet> tweets = Tweet.findByEventId( eventid );
        for( Tweet tweet : tweets ) {
            if( "positive".equals(tweet.sentiment()) )
                ++positive;
            if( "negative".equals(tweet.sentiment()) )
                ++negative;
            if( "neutral".equals(tweet.sentiment()) )
                ++neutral;
        }
        tweets.clear();
        negativeSentiment.put( eventid, ((float)negative) / (positive + negative + neutral) );
        positiveSentiment.put( eventid, ((float)positive) / (positive + negative + neutral) );
    }

    public static List<NewsArticle> all() {
        ArrayList<NewsArticle> articles = new ArrayList<NewsArticle>();
        try {
            String query = "SELECT id, url, title, text, date, city, state, newspaper_id, query, video_id, traffic_score, crime_score, protest_score FROM news";
            PreparedStatement prepStatement = SQLConnection.getConnection().prepareStatement(query);
            ResultSet result = prepStatement.executeQuery();
            while( result.next() )
                articles.add( new NewsArticle(result) );
        } catch( Exception e ) {
            e.printStackTrace();
        }
        return articles;
    }
}
