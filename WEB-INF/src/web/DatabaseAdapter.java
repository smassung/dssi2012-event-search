package web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import Geo.Geo.Geo;


/**
 * Servlet implementation class DatabaseAdapter
 */
public class DatabaseAdapter extends HttpServlet {
	private static final long serialVersionUID = 2L;
    
    private static Directory directory;
    private static Analyzer analyzer;
    private static IndexReader reader;
    private static IndexSearcher searcher;
    private static QueryParser parser;
    
    private String search_query = "";
    private String search_subtopic = "";
    private int count_so_far = 0;
    private int[] counts_so_far = null;
    private ArrayList<Document> pos_docs = null;
    private ArrayList<Document> neg_docs = null;
    private ArrayList<Document> neu_docs = null;
    private List<Document> docs = null;
    
    private ArrayList<Double> lats = null;
    private ArrayList<Double> lons = null;
    private ArrayList<String> lltext = null;
    
    private static SimpleHTMLFormatter formatter;

    
    public static void connect() throws IOException {
        connect("/data/lucene/index");
    }
    
    
    public static void connect( String location ) throws IOException {
        System.out.println( location );
        analyzer = new EnglishAnalyzer( Version.LUCENE_36 );
        directory = FSDirectory.open( new File(location) );
        reader = IndexReader.open( directory );
        searcher = new IndexSearcher( reader );
        parser = new QueryParser( Version.LUCENE_36, "searchText", analyzer );
        formatter = new SimpleHTMLFormatter();
    }
    
    
    public static List<Document> search( String query ) throws ParseException, IOException {
        ArrayList<Document> docs = new ArrayList<Document>();
        Query lucQuery = parser.parse( QueryParser.escape(query) );
        ScoreDoc[] hits = searcher.search( lucQuery, null, 1000 ).scoreDocs;
        for( ScoreDoc scoreDoc : hits )
            docs.add( searcher.doc( scoreDoc.doc ) );
        return docs;
    }
    
    public static List<Document> searchBySubEvent( String query, String subEvent ) throws ParseException, IOException {
        ArrayList<Document> docs = new ArrayList<Document>();
        String realQuery = "searchText:" + QueryParser.escape(query) + " AND subEvent:" + QueryParser.escape(subEvent);
        Query lucQuery = parser.parse( realQuery );
        ScoreDoc[] hits = searcher.search( lucQuery, null, 1000 ).scoreDocs;
        for( ScoreDoc scoreDoc : hits ) {
            Document doc = searcher.doc( scoreDoc.doc );
            doc.add( new NumericField("docid").setIntValue( scoreDoc.doc ) );
            docs.add( doc );
        }
        return docs;
    }
    
    
    public static float getSentiment( Document document ) {
        return ( (NumericField)document.getFieldable("sentiment") ).getNumericValue().floatValue();
    }
    
    public static float getPositiveSentiment( Document document ) {
        return ( (NumericField)document.getFieldable("positive") ).getNumericValue().floatValue();
    }
    
    
    public static String getSnippet( Document document, String query ) throws ParseException, IOException, InvalidTokenOffsetsException {
        return getSnippet( document, query, 6 );
    }

    public static String getSnippet( Document document, String query, int numFragments ) throws ParseException, IOException, InvalidTokenOffsetsException {
        Query lucQuery = parser.parse( QueryParser.escape(query) );
        Highlighter highlighter = new Highlighter( formatter, new QueryScorer(lucQuery) );
        TokenStream tokenStream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), getDocId(document), "fullText", analyzer);
        return highlighter.getBestFragments( tokenStream, document.get("fullText"), numFragments, "..." );
    }
    
    public static int getDocId( Document document ) {
        return ( (NumericField)document.getFieldable("docid") ).getNumericValue().intValue();
    }
    
    
    public DatabaseAdapter() {
        super();
    }
    
    public void closeConnection(){
    	
    }
    
    Logger lgr = Logger.getLogger(DatabaseAdapter.class.getName());
    public void init(){

    	try{
    		connect();
    		Geo geo = new Geo();
    	}
    	catch(IOException e){
    		lgr.log(Level.WARNING, e.getMessage(), e);
    	}
    	lgr.log(Level.INFO, "INIT FINISH", new String("INIT FINISH"));
    }
    
    
    public String truncate(String input, int limit){
    	int index = input.indexOf("Embed this video");
    	if(index <= 0) index = input.length();
    	input = input.substring(0, index);
    	if(input.length() > limit){
    		return input.substring(0, limit)+" ...more";
    	}
    	else return input;
    }
    
    
    public void addLocaltion() throws IOException{
		
		lats = new ArrayList<Double>();
		lons = new ArrayList<Double>();
		lltext = new ArrayList<String>();
		
		if(docs != null){
			for(Document doc : docs){
	    		String location = doc.get("location");
	    		Geo.ReadTweetByString("Hello " + location);
	    		String word = null;
	    		while((word = Geo.GetNextWordFromTweet())!=null){
	    			double[] ll = Geo.GetNextGeoLocationFromTweet();
	    			lats.add(ll[0]);
	        		lons.add(ll[1]);
	        		lltext.add(doc.get("title"));
	        		break;
	    		}
	    		
	    	}
		}
    }
    
    public int getSentiment(){
    	Random r = new Random();
    	double result = r.nextDouble();
    	if(result <= 0.25) return 0;		//0 for happy
    	else if(result <= 0.5) return 1;	//1 for sad
    	else return 2;						//2 for neutral
    }
    
    public void getEntireText(String query, PrintWriter out) throws IOException{
    	out.print("HELLO THIS IS ENTIRE TEXT!");
    	out.close();
    }
    
    
    public void getGeo(String query, PrintWriter out) throws IOException{

    	addLocaltion();
    	
        for(int i=0; i<lats.size(); i++){
        	Random r = new Random();
        	
         	out.print(i+"``"+(lats.get(i)+r.nextDouble()*0.1-0.05)+
         			"``-"+(lons.get(i)+r.nextDouble()*0.1-0.05)+"``"+lltext.get(i)+"##");
         }
     	out.close();
 		
    }
    
    
    public void getChart(String query, PrintWriter out) throws IOException{
    	
    		Random r = new Random();
    		int total = pos_docs.size() + neu_docs.size() + neg_docs.size();
    		int pos = (int)(Math.floor(pos_docs.size()*100.0 / total));
    		int neg = (int)(Math.floor(neg_docs.size()*100.0 / total));
    		int neu = 100-pos-neg;
    		out.print(0+"``Positive``"+pos+"##");
    		out.print(1+"``Negative``"+neg+"##");
    		out.print(2+"``Neutral``"+neu+"##");
    		out.close();
    }
    
    
    protected void writeEventResponse(String query, int k, List<Document> d, PrintWriter out, int sentiment, int limit) throws IOException, ParseException, InvalidTokenOffsetsException{
    	
    	int upper = counts_so_far[k]+limit > d.size() ? d.size() : counts_so_far[k]+limit;
        for(int i=counts_so_far[k]; i<upper; i++){
        	Document doc = d.get(i);
        	//double senti = ((NumericField)doc.getFieldable("sentiment")).getNumericValue().floatValue(); 
            out.print(i+"``"+sentiment+"``"+doc.get("title")+"``"+
            		getSnippet(doc, query)+"``"+doc.get("url")+"##");
        }
        counts_so_far[k] = upper;
    }
    
    protected void distributeSenti(){
    	counts_so_far = new int[3];
    	count_so_far = 0;
    	
    	pos_docs = new ArrayList<Document>();
    	neg_docs = new ArrayList<Document>();
    	neu_docs = new ArrayList<Document>();
    	
    	HashSet<String> dupli_set = new HashSet<String>();
    	
    	for(Document doc : docs){
    		String title = doc.get("title");
    		if(!dupli_set.contains(title)){
    			float senti = getSentiment(doc);
        		if(senti > 0.3) neg_docs.add(doc);
        		else if(senti > 0.15) neu_docs.add(doc);
        		else pos_docs.add(doc);
        		dupli_set.add(new String(title));
    		}
    	}
    	
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String query = request.getQueryString();

        char label = query.charAt(0);
        char direction = query.charAt(query.length()-1);
        String subtopic = "";
        query = query.substring(1, query.length()-1);
        
                
        if(label == '0') subtopic = "traffic";
        else if(label == '1') subtopic = "protest";
        else if(label == '2') subtopic = "crime";
        else if(label == '3'){
        	getEntireText(query, out);
        	return;
        }
        else if(label == 'g'){
        	getGeo(query, out);
        	return;
        }
        else if(label == 'c'){
        	getChart(query, out);
        	return;
        }
        
        int sentiment = 0;
        boolean random_senti = false;
        if(direction == '0') random_senti = true;
        else if(direction == '1') sentiment = 0;
        else if(direction == '2') sentiment = 1;
        else if(direction == '3') sentiment = 2;
        else sentiment = 3;
        
        query = query.replaceAll("%20", " ");
        query = query.replaceAll(" +", " ");
        char force = query.charAt(query.length()-1);
        query = query.substring(0, query.length()-1);
        //out.print("0``0``"+query+"##");
        
        if(!query.equals(search_query) || !subtopic.equals(search_subtopic) || force == '1'){
        	 try{
             	if(subtopic.equals("")) docs = search(query);
             	else docs = searchBySubEvent(query, subtopic);
             	distributeSenti();
        	 }
             catch(Exception e){
             	e.printStackTrace();
             }
        	 
        	 search_query = new String(query);
        	 search_subtopic = new String(subtopic);
        }
        
        if(random_senti){
        	
        	try {
        		writeEventResponse(query, 0, pos_docs, out, 0, 3);
        		writeEventResponse(query, 1, neu_docs, out, 1, 2);
				writeEventResponse(query, 2, neg_docs, out, 2, 3);
				writeEventResponse(query, 1, neu_docs, out, 3, 2);
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (InvalidTokenOffsetsException e) {
				e.printStackTrace();
			}
        	
        	out.close();
        }
        else{
				try {
					if(sentiment == 0) writeEventResponse(query, 0, pos_docs, out, sentiment, 5);
					else if(sentiment == 2) writeEventResponse(query, 2, neg_docs, out, sentiment, 5);
		        	else writeEventResponse(query, 1, neu_docs, out, sentiment, 5);
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (InvalidTokenOffsetsException e) {
					e.printStackTrace();
				}
			
        	out.close();
        }
        
        
        
        	
//        int upper = count_so_far+20 > docs.size() ? docs.size() : count_so_far+20;
//        for(int i=count_so_far; i<upper; i++){
//        	Document doc = docs.get(i);
//        	//double senti = ((NumericField)doc.getFieldable("sentiment")).getNumericValue().floatValue(); 
//            out.print(i+"``"+"0``"+doc.get("title")+doc.get("sentiment")+"##");
//        }
//        count_so_far = upper;
//        out.close();
       
        
//        try {
//        	if(st == null){
//        		out.print("NULL");
//        		out.close();
//        	}
//        	
//        	else{
//        		rs = st.executeQuery("SELECT `id`,`text`,`query`,`subtopic` FROM `news_training` WHERE `query`=\""+query + "\" AND `subtopic`=\"" + subtopic + "\"LIMIT 0,30");
//        		
//            	while(rs.next()){
//    				String text = truncate(rs.getString("text"), 300);
//    				
//    				if(random_senti) sentiment = getSentiment();
//    				out.print(rs.getString("id") + "``" + sentiment + "``" + text + "##");
//    			}
//    			out.close();
//        	}
//        	
//			
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
        
        
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
