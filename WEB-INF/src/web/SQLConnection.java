package web;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Class that encapuslates our sql connections.
 * 
 * @author Chase Geigle
 */
public class SQLConnection {
    private static Connection connection;
    private static boolean connected = false;

    public static boolean connect() {
        String url = "jdbc:mysql://engr-dssi01.engr.illinois.edu:3306/entitysearch";
        String user = "entitysearch";
        String password = "hello_dssi";

        try {
            connection = DriverManager.getConnection(url, user, password);
            connected = true;
            return true;
        } catch( Exception e ) {
            e.printStackTrace();
            return false;
        }
    }

    public static Connection getConnection() {
        if( !connected )
            connect();
        return connection;
    }
}
