package crawl;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;

import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.en.EnglishAnalyzer;

/**
 * Defines a common document class for social media sources. Primarily used
 * as a POD class for the Twitter, Blogger, etc source providers for doing
 * sentiment analysis.
 * 
 * @author Chase Geigle
 */
public class SMDocument {
    private String body;
    private String sentiment;
    private List<String> comments; // may be null in the case of tweets, 
                                   // etc that do not have a notion of comments

    /**
     * Setter for the body of the document.
     * 
     * @param body The body to be set.
     * @return The current instance (to support method chaining).
     */
    public SMDocument setBody( String body ) {
        this.body = body;
        return this;
    }

    /**
     * Setter for the sentiment label of the document.
     * 
     * @param sentiment The new sentiment label.
     * @return The current instance (to support method chaining).
     */
    public SMDocument setSentiment( String sentiment ) {
        this.sentiment = sentiment;
        return this;
    }

    /**
     * Adds a comment to the list of comments for this document.
     * 
     * @param comment Body of the comment to be added.
     * @return The current instance (to support method chaining).
     */
    public SMDocument addComment( String comment ) {
        initializeCommentsList();
        if( comments == null )
            comments = new ArrayList<String>();
        comments.add( comment );
        return this;
    }

    /**
     * Bulk adder for comments for this document.
     * 
     * @param comments java.util.List of comments to be added.
     * @return The current instance (to support method chaining).
     */
    public SMDocument addComments( List<String> comments ) {
        initializeCommentsList();
        this.comments.addAll( comments );
        return this;
    }

    /**
     * Getter for the body text of this document.
     * 
     * @return Body of this document.
     */
    public String getBody() {
        return this.body;
    }

    /**
     * Getter for the clean body text of this document.
     *
     * TODO: Make this more general---right now includes many
     * twitter-specific things.
     *
     * @return A cleaned up body of this document.
     */
    public String getCleanBody() {
        // replace at symbols
        String cleanBody = body.replaceAll("@\\S+", "|T|");


        // replace hashtags
        //cleanBody = cleanBody.replaceAll("#\\S+", "|H|");

        // replace URLs
        String[] words = cleanBody.split("\\s");
        StringBuilder cleanedBody = new StringBuilder();
        for( String word : words ) try {
            @SuppressWarnings("unused")
			URL url = new URL(word);
            cleanedBody.append("|URL| ");
        } catch( MalformedURLException e ) {
            cleanedBody.append(word + " ");
        }

        cleanBody = cleanedBody.toString().trim();
        cleanBody = cleanBody.replaceAll("!", " |EXC|");
        cleanBody = cleanBody.replaceAll("([a-zA-Z])\\1+", "$1$1");
        EnglishAnalyzer englishAnalyzer = new EnglishAnalyzer(Version.LUCENE_36);
        QueryParser qp = new QueryParser(Version.LUCENE_36, "", englishAnalyzer);
        try {
            String escaped = QueryParser.escape(cleanBody.toLowerCase());
            cleanBody = qp.parse(escaped).toString();
        } catch( ParseException e ) {
            e.printStackTrace();
        }
        return cleanBody;
    }

    /**
     * Getter for the comments for this document. Returns an empty list in
     * the case where there are no comments added to this document.
     *
     * @return List of comments added to this document, or the empty list.
     */
    public List<String> getComments() {
        if( comments == null )
            return new ArrayList<String>();
        return comments;
    }

    /**
     * Getter for the sentiment label for this document.
     * 
     * @return Sentiment for the document.
     */
    public String getSentiment() {
        if( sentiment == null )
            return "unknown";
        return sentiment;
    }

    /**
     * Lazy-loading for the comments list: only called if we are going to
     * add a comment the list.
     */
    private void initializeCommentsList() {
        if( comments == null )
            comments = new ArrayList<String>();
    }
}
