package crawl;

/**
 * Interface that defines the useage of a source of social media documents.
 * 
 * @author Chase Geigle
 */
public interface SocialMediaSource {
    /**
     * Sets the current query to use for this social media source.
     *
     * @param query The query that we want to search using.
     */
    public void setQuery( String query );

    /**
     * Grabs the next result for the current query, if one exists. Returns
     * null if there are no more results to be found.
     * 
     * @return The next document matching the query result, or null if one
     * does not exist.
     */
    public SMDocument getNext();
}
