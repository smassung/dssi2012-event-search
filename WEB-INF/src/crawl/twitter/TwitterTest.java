package crawl.twitter;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import crawl.SMDocument;

/**
 * Testing class for the TwitterSource.
 * 
 * @author Chase Geigle
 */
public class TwitterTest {
    /**
     * Testing method for the TwitterSource. Lazy-man's method for handling
     * exceptions: throw them all!
     */
    public static void main( String[] args ) throws Exception {
        TwitterSource source = new TwitterSource();
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        while( true ) {
            System.out.print("Query: ");
            String query = stdin.readLine();
            if( query.isEmpty() )
                break;
            source.setQuery( query );
            while( true ) {
                SMDocument tweet = source.getNext();
                if( tweet == null )
                    break;
                System.out.println( tweet.getBody() );
                System.out.println( tweet.getCleanBody() );
                System.out.println( "================" );
                System.out.println("Continue? (Y/n)");
                if( "n".equals(stdin.readLine()) )
                    break;
            }
        }
    }
}
