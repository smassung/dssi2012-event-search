package crawl.twitter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;

import twitter4j.FilterQuery;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

/**
 * Twitter Stream reader.
 * 
 * @author Chase Geigle
 */
public class TwitterStreamReader {
    public static void main( String[] args ) throws Exception {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Output file: ");
        final String output = stdin.readLine();

        // grab query terms
        ArrayList<String> track = new ArrayList<String>();
        while( true ) {
            System.out.print("Query term: ");
            String line = stdin.readLine();
            if( line.isEmpty() )
                break;
            track.add(line);
        }

        System.out.println("Starting reader...");

        FilterQuery twitQuery = new FilterQuery().track(track.toArray(new String[track.size()]));

        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        StatusListener listener = new StatusListener() {

            private int numCollected = 0;
            private BufferedWriter fileWriter = new BufferedWriter(new FileWriter(output));

            public void onStatus( Status status ) {
                ++numCollected;
                if( numCollected % 100 == 0 )
                    System.out.println( numCollected + " collected..." );
                try {
                    fileWriter.write( status.getText() );
                    fileWriter.newLine();
                } catch( IOException e ) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }

            public void onException( Exception e ) {
                e.printStackTrace();
            }

            public void onDeletionNotice( StatusDeletionNotice sdn ) {
                // don't care
            }

            public void onTrackLimitationNotice( int blah ) {
                // don't care
            }

            public void onScrubGeo( long lat, long lon ) {
                // don't care
            }
        };
        twitterStream.addListener(listener);
        twitterStream.filter(twitQuery);
    }
}
