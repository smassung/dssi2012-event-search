package crawl.twitter;

import java.util.Iterator;

import crawl.SMDocument;
import crawl.SocialMediaSource;

import twitter4j.Query;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Social Media Source for Twitter data. Uses the twitter4j library to do
 * search queries against the twitter search api.
 * 
 * @author Chase Geigle
 */
public class TwitterSource implements SocialMediaSource {
    private String queryString;
    private int page;
    private Iterator<Tweet> queryIterator;
    private boolean runQuery;
    private Twitter twitterAPI;

    /**
     * Constructs a new TwitterSource instance. 
     */
    public TwitterSource() {
        runQuery = false;
        twitterAPI = new TwitterFactory().getInstance();
    }

    /**
     * {@inheritDoc}
     * @see SocialMediaSource#setQuery(String)
     */
    public void setQuery( String query ) {
        this.queryString = query;
        runQuery = true;
        page = 1;
    }

    /**
     * {@inheritDoc}
     * @see SocialMediaSource#getNext()
     */
    public SMDocument getNext() {
        try {
            lazyRunQuery();
            if( queryIterator != null && queryIterator.hasNext() )
                return new SMDocument().setBody(queryIterator.next().getText());
            else {
                runQuery = true;
                lazyRunQuery();
                if( queryIterator != null && queryIterator.hasNext() )
                    return new SMDocument().setBody(queryIterator.next().getText());
                else
                    return null;
            }
        } catch( TwitterException te ) {
            te.printStackTrace();
            return null;
        }
    }

    /**
     * Lazily runs the query agaist the twitter API.
     * 
     * @throws TwitterException
     */
    private void lazyRunQuery() throws TwitterException {
        if( !runQuery )
            return;
        Query query = new Query( this.queryString );
        System.out.println("Page: " + page);
        query.page( page )
            .lang( "en" )
            .rpp( 100 );
        queryIterator = twitterAPI.search(query).getTweets().iterator();
        runQuery = false;
        ++page;
    }
}
