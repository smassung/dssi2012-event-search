package crawl.twitter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;

import crawl.SMDocument;

/**
 * Utility to save twitter search results so we don't lose them in
 * twitter's stupid one week limit...
 * 
 * @author Chase Geigle
 */
public class TwitterSearchSaver {
    public static void main( String[] args ) throws Exception {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        while( true ) {
            System.out.print("Output: ");
            String outputFile = stdin.readLine();
            if( outputFile.isEmpty() )
                break;
            BufferedWriter output = new BufferedWriter(new FileWriter(outputFile));
            TwitterSource source = new TwitterSource();
            System.out.print("Query: ");
            source.setQuery( stdin.readLine() );
            int numTweets = 0;
            while( true ) {
                SMDocument tweet = source.getNext();
                if( tweet == null )
                    break;
                if( tweet.getBody().matches("RT") )
                    continue;
                output.write(tweet.getBody());
                output.newLine();
                if( ++numTweets % 10 == 0 )
                    System.out.println(numTweets + " gathered...");
            }
            output.flush();
            output.close();
            System.out.println("Finishing...");
        }
        System.out.println("Exiting...");
    }
}
