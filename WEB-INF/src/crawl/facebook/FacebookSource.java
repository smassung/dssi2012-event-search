package crawl.facebook;

import java.util.Iterator;
import java.util.List;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;

import com.restfb.types.Comment;
import com.restfb.types.Post;

import crawl.SMDocument;
import crawl.SocialMediaSource;

/**
 * Social Media Source for Facebook data.
 * 
 * @author Chase Geigle
 */
public class FacebookSource implements SocialMediaSource {
    private String queryString;
    private FacebookClient client;
    private Iterator<List<Post>> postListIterator;
    private Iterator<Post> postIterator;

    public FacebookSource() {
        client = new DefaultFacebookClient(); // we only search for statuses that are public
                                              // silly people not turning on privacy settings...
    }

    public void setQuery( String query ) {
        this.queryString = query;
        postListIterator = null;
    }

    public SMDocument getNext() {
        lazyRunQuery();
        if( !postIterator.hasNext() ) {
            // determine if we need to move to page 2
            if( !postListIterator.hasNext() )
                return null;
            else
                postIterator = postListIterator.next().iterator();
        }
        // advance iterator
        Post currPost = postIterator.next();

        // construct return document
        SMDocument retDoc = new SMDocument();
        // add the body as the body of this status
        String body = currPost.getMessage();
        if( body == null )
            body = "";
        /*
        if( currPost.getLink() != null ) {
            body += "\n" + currPost.getLink();
            if( currPost.getCaption() != null )
                body += "\n" + currPost.getCaption();
            if( currPost.getDescription() != null )
                body += "\n" + currPost.getDescription();
        }
        */
        if( body.isEmpty() )
            return getNext();
        retDoc.setBody( body );
        // individually add every comment of this status as comments on the
        // document
        if( currPost.getComments() != null )
            for( Comment comment : currPost.getComments().getData() )
                retDoc.addComment( comment.getMessage() );
        return retDoc;
    }

    private void lazyRunQuery() {
        if( postListIterator == null ) {
            postListIterator = client.fetchConnection( 
                    "search", 
                    Post.class, 
                    Parameter.with("q", queryString), 
                    Parameter.with("type", "post") 
                ).iterator();
            postIterator = postListIterator.next().iterator();
        }
    }
}
