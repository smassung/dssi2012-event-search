package crawl.facebook;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import crawl.SMDocument;

/**
 * Testing application for the Facebook social media source.
 * 
 * @author Chase Geigle
 */
public class FacebookTest {
    public static void main( String[] args ) throws Exception {
        FacebookSource fbSource = new FacebookSource();
        BufferedReader stdin = new BufferedReader( new InputStreamReader(System.in) );
        System.out.print("Query: ");
        String query = stdin.readLine();
        fbSource.setQuery( query );
        while( true ) {
            SMDocument doc = fbSource.getNext();
            if( doc == null )
                break;
            System.out.println( doc.getBody() );
            for( String comment : doc.getComments() )
                System.out.println( "> " + comment );
            System.out.println("============");
            System.out.println("Continue? (Y/n)");
            if( "n".equals(stdin.readLine()) )
                break;
        }
    }
}
