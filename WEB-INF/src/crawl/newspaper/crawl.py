import urllib2
import sys
import os
from include.config import MySQL, BingSearch, FindDate, getURLText, GetComments
import jpype

# Set up JVM for boilerpipe
classpath = "include/boilerpipe/boilerpipe-1.2.0.jar:include/boilerpipe/nekohtml-1.9.13.jar:include/boilerpipe/xerces-2.9.1.jar:classifier/news/weka.jar:.:$PATH"
jpype.startJVM(jpype.getDefaultJVMPath(), "-Djava.class.path=%s" % classpath)
ArticleExtractor = jpype.JPackage("de").l3s.boilerpipe.extractors.ArticleExtractor
Classifier = jpype.JPackage("classifier").news.NewsClassifier()

# Connect to DB
mysql = MySQL()
db = mysql.connect()
cursor = db.cursor()
grabcur = db.cursor()
 
# Get Bing search
bing = BingSearch()

# Get date finder
findDate = FindDate()

# Get comment finder
gc = GetComments()

# Get topic
topic = str(raw_input("Enter query string: "))
test_mode = str(raw_input("Blank for live, 1 for display: "))

# Get all sources
grabq = "SELECT * FROM papers ORDER BY RAND() LIMIT 3000"
grabcur.execute(grabq)

# Run the queries on each paper
for paper in grabcur.fetchall():
  try:
    paperID = int(paper[0])
    paperURL = paper[1]
    city = paper[2]
    state = paper[3]

    print "Crawling " + paperURL + "..."

    query = topic + " site:" + paperURL
    results = bing.search(query, result_count=50)

    #print len(results)
    for result in results:
      page_title = result['title']
      page_link = str(result['link'])
      page_newspaper = paperURL
      page_html = getURLText(page_link)
      page_text = ArticleExtractor.INSTANCE.getText(page_html)
      page_date = findDate.finddate(page_html)

      # We don't want to insert blank records
      # from when Boilerpipe is unsuccessful
      if len(page_text) < 200:
        continue

      protestScore = Classifier.testProtest(page_text)
      crimeScore = Classifier.testCrime(page_text)
      trafficScore = Classifier.testTraffic(page_text)

      if test_mode == "1":
        print "    ---------------------------------------"
        print "    Title: " + page_title
        if page_date:
          print "    Date: " + page_date
        print "    Body: " + page_text[:200] + "..."
      else: 
        # Insert to DB
        insert_data = ( (page_link, page_title, page_text, page_date, paperID, city, state, topic, protestScore, crimeScore, trafficScore, page_link), )
        query = "INSERT IGNORE INTO news (url, title, text, date, newspaper_id, city, state, query, protest_score, crime_score, traffic_score, url_md5) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, UNHEX(MD5(%s)))"
        # Execute query
        cursor.executemany(query, insert_data)

        # Now get the new id to run the comments script
        query = "SELECT id FROM news WHERE url='"+page_link+"' LIMIT 1"
        cursor.execute(query)

        for row in cursor.fetchall():
          pageID = row[0]
          gc.insertComments(pageID, page_html, page_link)

  # Most errors encountered in development have been taken care of
  # If new errors occur during crawling, we will just skip that source
  # and continue so that the script will run without stopping.
  except Exception as e:
    print e
    if test_mode == "1":
      sys.exit(e)
    continue

# Close the db
db.close()
