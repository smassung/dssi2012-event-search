import urllib2
import sys
import os
from include.config import MySQL
import jpype

# Set up JVM for boilerpipe
classpath = "include/boilerpipe/boilerpipe-1.2.0.jar:include/boilerpipe/nekohtml-1.9.13.jar:include/boilerpipe/xerces-2.9.1.jar:classifier/news/weka.jar:.:$PATH"
jpype.startJVM(jpype.getDefaultJVMPath(), "-Djava.class.path=%s" % classpath)
Classifier = jpype.JPackage("classifier").news.NewsClassifier()

# Connect to DB
mysql = MySQL()
db = mysql.connect()
cursor = db.cursor()
grabcur = db.cursor()
 
# Get all sources
grabq = "SELECT id,text FROM news"
grabcur.execute(grabq)


# Count how fast this is going
count = 0

# Run the queries on each paper
for article in grabcur.fetchall():
  count += 1
  if count in xrange(0,100000,250):
    print count

  try:
    articleID = str(article[0])
    page_text = unicode(article[1])

    protestScore = Classifier.testProtest(page_text)
    crimeScore = Classifier.testCrime(page_text)
    trafficScore = Classifier.testTraffic(page_text)

    query = "UPDATE news SET protest_score="+str(protestScore)+", crime_score="+str(crimeScore)+", traffic_score="+str(trafficScore)+" WHERE id="+articleID
    cursor.execute(query)

  # Most errors encountered in development have been taken care of
  # If new errors occur during crawling, we will just skip that source
  # and continue so that the script will run without stopping.
  except Exception as e:
    print e
    continue

# Close the db
db.close()
