import mysql.connector
import ajaxgoogle
import pybing
import feedparser
import time
import datetime
import os
import re
from string import replace
import urllib2
import json
import socket

def getURLText(url):
  try:
    text = urllib2.urlopen(url, timeout=5)
    text = text.read()
  except socket.timeout as e:
    text = ""
    return text
  except urllib2.HTTPError as e:
    text = ""
    return text
  except urllib2.URLError as e:
    text = ""
    return text

  text = unicode(text, errors="ignore")
  return text

class MySQL:

  # Set database parameters here
  dbconf = { 
    'host': 'engr-dssi01.engr.illinois.edu',
    'database': 'entitysearch',
    'user': 'entitysearch',
    'password': 'hello_dssi',
    'charset': 'utf8',
    'use_unicode': True,
    'get_warnings': True,
    'port': 3306,
    'buffered': True,
    }

  def __init__(self):
    pass
  
  # Returns the db connection
  def connect(self):
  
    try:
      db = mysql.connector.Connect(**self.dbconf)
    except Exception:
      # If this is running from engr-dssi01, need to use localhost
      self.dbconf['host'] = 'localhost'
      db = mysql.connector.connect(**self.dbconf)
    
    return db


class BingSearch:
  # Uses pybing
  bingAPI = "64BC99BBA2123E01F7192E2D28840092730DA909"

  def __init__(self):
    self.bing = pybing.Bing(self.bingAPI)



  # Searches Web for the indicated query, choose how many
  # total results to display and what record to start at
  def search(self, query, result_count=50, result_offset=0):
    response = self.bing.search(query, source_type=pybing.constants.WEB_SOURCE_TYPE, extra_params={'web.count': result_count, 'web.offset': result_offset})
    if response['SearchResponse']['Web']['Total'] == 0:
      return []
    response = response['SearchResponse']['Web']['Results']
    results = []
    for link in response:
      link_dict = {
          'link': link['Url'],
          'datetime': link['DateTime'],
          'title': link['Title']
          }

      results.append(link_dict)
    return results


class GoogleSearch:
  # Uses ajaxgoogle

  def __init__(self):
    pass

  def searchGoogle(query):
    response = agaxgoogle.search(query, rsz="large")
    results = []
    for link in response:
      link_dict = {
          'link': link['unescapedUrl'],
          'datetime': "",
          'title': link['title']
          }
   
      results.append(link_dict)
    return results

class FindDate:
    
  # Helper to format dates
  def formatDate(self, date):
    pydate = feedparser.parse("<feed><updated>"+date+"</updated></feed>")
    pydate = pydate.feed.date_parsed
    pydate = datetime.date.fromtimestamp(time.mktime(pydate))
    return pydate
  
  # Finds the date of an HTML news article
  def finddate(self, html):
  
    # Some common date formats
    pat1 = '(\d\d\d\d-\d\d-\d\d).\d\d:\d\d:\d\d'
    pat2 = '<meta name=\"(pub)?date\" content=\"(.*?)[ T]\d\d:\d\d:\d\d(.*?)\">'
    pat3 = '(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(uary|ruary|ch|il|e|y|ust|tember|ober|ember|\.)? (\d\d?)(, | )(\d\d\d\d)'
    pat4 = '(\d\d?)[/ ](\d\d?)[/ ](20)?(\d\d)'
    pat5 = '(\d\d?) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)(uary|ruary|ch|il|e|y|ust|tember|ober|ember|\.)?(, | )(\d\d\d\d)'
    pat6 = '(20\d\d)/(\d\d?)/(\d\d?)'

    todayDate = False
    count = 0
    small = 0
    nil = 0
    double = 0
    goodDate = False
    currDate = datetime.date.today()
    earlyBound = feedparser.parse("<feed><updated>2000-01-01</updated></feed>").feed.date_parsed
    earlyBound = datetime.date.fromtimestamp(time.mktime(earlyBound))
    date = None
    endOfDate = 0
    query = ""
    searchHTML = html

    # The best format, always take it if it exists
    res1 = re.search(pat1, searchHTML)
    if res1:
      date = res1.group(1)
      pydate = self.formatDate(date)
    else:
      # The other date formats aren't as good, so we'll try
      # ones that match and see if they're within our range.
      # We'll take the first one that's in range that isn't today.
      # (Since most sites have today's date somewhere)
      # If all else fails, default back to today, maybe it was
      # just published
      try:
        count = 0
        while not goodDate:
          if count > 100:
            if todayDate:
              return str(currDate.year)+"-"+str(currDate.month)+"-"+str(currDate.day)
            else:
              return None
          else:
            count += 1
          # Find the first date in the file
          res2 = re.search(pat2, searchHTML)
          res3 = re.search(pat3, searchHTML)
          res4 = re.search(pat4, searchHTML)
          res5 = re.search(pat5, searchHTML)
          res6 = re.search(pat6, searchHTML)
  
          found = []
          if res2:
            found.append(res2)
            res2end = res2.end(0)
          if res3:
            found.append(res3)
            res3end = res3.end(0)
          if res4:
            found.append(res4)
            res4end = res4.end(0)
          if res5:
            found.append(res5)
            res5end = res5.end(0)
          if res6:
            found.append(res6)
            res6end = res6.end(0)
  
          minEnd = 1000000
  
          for res in found:
            minEnd = min(minEnd,res.end(0))
  
 
          res2 = re.search(pat2, searchHTML[:minEnd])
          res3 = re.search(pat3, searchHTML[:minEnd])
          res4 = re.search(pat4, searchHTML[:minEnd])
          res5 = re.search(pat5, searchHTML[:minEnd])
          res6 = re.search(pat6, searchHTML[:minEnd])
        
          if res2:
            date = res2.group(1)
            newEndOfDate = res2end
          elif res3:
            date = res3.group(3)+" "+res3.group(1)+" "+res3.group(5)
            newEndOfDate = res3end
          elif res4:
            date = "20"+res4.group(4)+"-"+res4.group(1)+"-"+res4.group(2)
            newEndOfDate = res4end
          elif res5:
            date = res5.group(1)+" "+res5.group(2)+" "+res5.group(5)
            newEndOfDate = res5end
          elif res6:
            date = res6.group(1)+"-"+res6.group(2)+"-"+res6.group(3)
            newEndOfDate = res6end
          else:
            raise ValueError

          pydate = self.formatDate(date)
          if pydate < currDate and pydate > earlyBound:
            goodDate = True
          elif currDate == pydate:
            todayDate = True
            searchHTML = searchHTML[newEndOfDate:]
          else:
            searchHTML = searchHTML[newEndOfDate:]

      except ValueError:
        print "Error finding date in page"
        if todayDate:
          return str(currDate.year)+"-"+str(currDate.month)+"-"+str(currDate.day)
        else:
          return None
  
    # Build result
    date = str(pydate.year)+"-"+str(pydate.month)+"-"+str(pydate.day)
    return date
  
# Gets the comments (if any) for an article which uses the
# WorldNow content framework
class GetComments:
  partnerPat = "mdPartnerID : '(\d+)'"
  domainPat = "mdPartnerDomain : '(.*?)'"
  videoIDPat = "mdVideoOptions.video_id =\s+'(\d+)'"

  # Sets up the database connection
  def __init__(self):
    self.mysql = MySQL()
    self.db = self.mysql.connect()
    self.cursor = self.db.cursor()

  def __del__(self):
    self.db.close()

  # Finds comments for a given page and updates them in database
  def insertComments(self, articleID, article_html, articleURL):

    # Now search for partnerID, if they are using the worldnow framework this will be present
    partnerID = re.search(self.partnerPat, article_html)
    domainURL = re.search(self.domainPat, article_html)
    if partnerID and domainURL:
      partnerID = partnerID.group(1)
      domainURL = domainURL.group(1)
    else:
      return

    # Find the pageID from the URL
    pageID = ""
    splitURL = articleURL.split('/')
    # First case: /story/[pageID]/...
    for i in range(len(splitURL)):
      # Category pages dont have comments
      if splitURL[i] == "category":
        return
      if splitURL[i] == "story":
        pageID = splitURL[i+1]

    # Second case, ..?C=[pageID]
    if pageID == "":
      queryString = articleURL.split('?',1)
      if len(queryString) < 2:
        return
      queryString = queryString[1]
      queryString = queryString.split('&')
      for part in queryString:
        splitPart = part.split('=')
        partName = splitPart[0]
        partValue = splitPart[1]
        if partName == 'C' or partName == 'S':
          pageID = partValue

    print "Made it"

    # Now find the video_id field
    # First make the link
    modifiedURL = domainURL + "/partners/" + str(partnerID) + "/script.js?uri=s-" + str(pageID)

    # Now load and parse new page to get javascript
    pageJS = getURLText(modifiedURL)

    # Now search for the video_id
    video_id = re.search(self.videoIDPat, pageJS)
    if video_id:
      # If found, update database
      video_id = video_id.group(1)
      query = "UPDATE news SET video_id ="+str(video_id)+", comment_prefix='"+domainURL+"' WHERE id="+str(articleID)
      self.cursor.execute(query)
    else:
      return

    # Now find the actual comments
    # Build the JSON link
    jsonLink = domainURL + "/refs/" + video_id + "/smilTexts/comments.json"

    # Load and decode JSON link
    jsonText = urllib2.urlopen(jsonLink).read()

    jsonText = json.loads(jsonText)

    if jsonText['count'] > 0:
      print jsonText['count']
      for item in jsonText['items']:
        body = unicode(item['body'])
        insert_data = ( (articleID, body), )
        query = "INSERT INTO news_comments (articleID, body) VALUES (%s, %s)"
        self.cursor.executemany(query, insert_data)
    else:
      return

    return


