from include.config import MySQL, BingSearch, FindDate, getURLText
import jpype
import re



# Sorts out good sources from bad

# Set up JVM for boilerpipe
classpath = "include/boilerpipe/boilerpipe-1.2.0.jar:include/boilerpipe/nekohtml-1.9.13.jar:include/boilerpipe/xerces-2.9.1.jar"
jpype.startJVM(jpype.getDefaultJVMPath(), "-Djava.class.path=%s" % classpath)
ArticleExtractor = jpype.JPackage("de").l3s.boilerpipe.extractors.ArticleExtractor

# Connect to DB
mysql = MySQL()
db = mysql.connect()
grabcur = db.cursor()
cursor = db.cursor()

# Get Bing search
bing = BingSearch()

# Get date finding object
findDate = FindDate()

topic = "election"

# Filter out national articles
filter_pat = '(\(CNN\)|\(AP\)|Associated Press|CNN Wire|Reuters|BBC|%PDF|Page Not Found|some links have moved|Error 404)'

grabq = "SELECT * FROM tv_temp"
grabcur.execute(grabq)

for paper in grabcur.fetchall():
  paperURL = paper[1]
  city = paper[2]
  state = paper[3]
  hashedURL = paper[4]

  # Remove query to allow multiple users
  query = "DELETE FROM tv_temp WHERE url='"+paperURL+"'"
  # Execute query
  cursor.execute(query)

  print "Crawling " + paperURL + "..."
  
  query = topic + " site:" + paperURL
  results = bing.search(query)

  for result in results:
    page_title = result['title']
    page_link = unicode(result['link'])
    page_newspaper = paperURL
    page_html = getURLText(page_link)
    page_text = ArticleExtractor.INSTANCE.getText(page_html)
    page_date = findDate.finddate(page_html)

    res = re.search(filter_pat, page_text, re.IGNORECASE)
    if res:
      print page_text[res.start(0)-10:res.end(0)+10]
      continue
    elif len(page_text) < 100:
      print "EMPTY"
      continue

    print "    ---------------------------------------"
    print "    Title: " + page_title
    if page_date:
      print "    Date: " + page_date
    print "    Body: " + page_text[:750] + "..."
    print str(len(results)) + " total results from this paper"

    # Get user response
    response = str(raw_input("k to keep, n to skip, m for more, q to quit: "))
    if response == "k":

      # Insert to DB
      insert_data = ( (paperURL, city, state, paperURL), )
      query = "INSERT IGNORE INTO papers (url, city, state, url_md5) VALUES (%s, %s, %s, UNHEX(MD5(%s)))"
      # Execute query
      cursor.executemany(query, insert_data)
      break

    elif response == "n":
      break

    elif response == "m":
      continue

    else:
      # Put this one back in to look at later
      insert_data = ( (paperURL, city, state, paperURL), )
      query = "INSERT IGNORE INTO tv_temp (url, city, state, url_md5) VALUES (%s, %s, %s, UNHEX(MD5(%s)))"
      # Execute query
      cursor.executemany(query, insert_data)

      # Close the db
      db.close()
      exit()

  

# Close the db
db.close()

