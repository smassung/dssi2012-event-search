import urllib2
import sys
import os
from include.config import MySQL, BingSearch, FindDate, getURLText, GetComments
import jpype

# Connect to DB
mysql = MySQL()
db = mysql.connect()
cursor = db.cursor()
grabcur = db.cursor()
 
# Get all sources
grabq = "SELECT id,title,text FROM news"
grabcur.execute(grabq)

# Run the queries on each paper
for article in grabcur.fetchall():
  try:
    articleID = str(article[0])
    articleTitle = article[1]
    articleText = article[2]

    articleTitle = ''.join([x for x in articleTitle if ord(x) < 128])
    articleText = ''.join([x for x in articleText if ord(x) < 128])

    insert_data = ( (articleTitle, articleText, articleID), )
    query = "UPDATE news SET title=%s, text=%s WHERE id=%s"
    cursor.executemany(query, insert_data)

  # Most errors encountered in development have been taken care of
  # If new errors occur during crawling, we will just skip that source
  # and continue so that the script will run without stopping.
  except Exception as e:
    print e
    continue

# Close the db
db.close()
