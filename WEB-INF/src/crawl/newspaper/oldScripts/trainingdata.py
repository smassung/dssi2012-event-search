import urllib2
import os
from include.config import MySQL, BingSearch, FindDate
import jpype

def getURLText(url):
  try:
    text = urllib2.urlopen(url).read()
  except:
    text = ""
  return unicode(text, errors="ignore")

# Set up JVM for boilerpipe
classpath = "include/boilerpipe/boilerpipe-1.2.0.jar:include/boilerpipe/nekohtml-1.9.13.jar:include/boilerpipe/xerces-2.9.1.jar"
jpype.startJVM(jpype.getDefaultJVMPath(), "-Djava.class.path=%s" % classpath)
ArticleExtractor = jpype.JPackage("de").l3s.boilerpipe.extractors.ArticleExtractor

# Connect to DB
mysql = MySQL()
db = mysql.connect()
cursor = db.cursor()
grabcur = db.cursor()
 
# Get Bing search
bing = BingSearch()

# Get date finder
findDate = FindDate()

# Two sample topics
topic = str(raw_input("Say topic (e.g. super bowl): "))
subtopic = str(raw_input("Say subtopic (e.g. crime): "))
newspaper = str(raw_input("Say source (e.g. indystar.com, can be blank): "))
result_count = raw_input("Say result count (default 50): ")
result_offset = raw_input("Say result offset (default 0): ")

if result_count == "":
  result_count = 50
else:
  result_count = int(result_count)

if result_offset == "":
  result_offset = 0
else:
  result_offset = int(result_offset)

if len(newspaper) > 0:
  query = topic+' '+subtopic+' site:'+newspaper
else:
  query = topic+subtopic

results = bing.search(query, result_count=result_count, result_offset=result_offset)

for result in results:
  page_title = result['title']
  page_link = unicode(result['link'])
  page_newspaper = newspaper
  page_html = getURLText(page_link)
  page_text = ArticleExtractor.INSTANCE.getText(page_html)
  page_date = findDate.finddate(page_html)

  print "    ---------------------------------------"
  print "    Url: " + page_link
  print "    Title: " + page_title
  print "    Body: " + page_text[:750] + "..."

  # Get user response
  response = str(raw_input("k to keep, o for other, n to skip, q to quit: "))
  if response == "k":

    # Insert to DB
    insert_data = ( (page_link, page_title, page_html, page_text, topic, subtopic, page_date, page_link), )
    query = "INSERT IGNORE INTO news_training (url, title, html, text, query, subtopic, date, url_md5) VALUES (%s, %s, %s, %s, %s, %s, %s, UNHEX(MD5(%s)))"
    # Execute query
    cursor.executemany(query, insert_data)

  elif response == "o":
    # Insert to DB
    insert_data = ( (page_link, page_title, page_html, page_text, topic, "other", page_date, page_link), )
    query = "INSERT IGNORE INTO news_training (url, title, html, text, query, subtopic, date, url_md5) VALUES (%s, %s, %s, %s, %s, %s, %s, UNHEX(MD5(%s)))"
    # Execute query
    cursor.executemany(query, insert_data)


  elif response == "n":
    continue

  else:
    break


db.close()
