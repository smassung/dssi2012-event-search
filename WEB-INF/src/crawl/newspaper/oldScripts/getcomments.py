import urllib2
import sys
import re
import os
from include.config import MySQL, FindDate, getURLText

# Connect to DB
mysql = MySQL()
db = mysql.connect()
cursor = db.cursor()
grabcur = db.cursor()
 
grabq = "SELECT news.id,news.url,papers.url FROM news,papers WHERE news.newspaper_id = papers.id AND source_type='tv'"
grabcur.execute(grabq)

partnerPat = "mdPartnerID : '(\d+)'"
domainPat = "mdPartnerDomain : '(.*?)'"
videoIDPat = "mdVideoOptions.video_id =\s+'(\d+)'"

count = 0

# Run the queries on each paper
for link in grabcur.fetchall():
    articleID = int(link[0])
    articleURL = str(link[1])
    sourceURL = str(link[2])

    count += 1
    if count in [250,500,750,1000,1250,1500,1750,2000,2250,2500,2750,3000,3500,4000,4500,5000]:
      print count
    
    # Find the article's HTML
    #print "Crawling " + articleURL + "..."
    articleHTML = getURLText(articleURL)
    
    # Find the partnerID first, if it is using the right comment system
    partnerID = re.search(partnerPat, articleHTML)
    domainURL = re.search(domainPat, articleHTML)
    if partnerID and domainURL:
      partnerID = partnerID.group(1)
      domainURL = domainURL.group(1)

      # Find the pageID from the URL
      pageID = ""
      splitURL = articleURL.split('/')
      # First case: /story/[pageID]/...
      for i in range(len(splitURL)):
        if splitURL[i] == "category":
          pageID = "Error" 
        if splitURL[i] == "story":
          pageID = splitURL[i+1]
      
      # Second case, ...?C=[pageID]
      if pageID == "":
        queryString = articleURL.split('?',1)
        if len(queryString) < 2:
          continue
        queryString = queryString[1]
        queryString = queryString.split('&')
        for part in queryString:
          splitPart = part.split('=')
          partName = splitPart[0]
          partValue = splitPart[1]
          if partName == 'C' or partName == 'S':
            pageID = partValue

      # Here we have an error
      if pageID == "Error":
        continue
      if pageID == "":
        print articleURL

      
      # Now find the video_id field
      # First make the link
      modifiedURL = domainURL + "/partners/" + str(partnerID) + "/script.js?uri=s-" + str(pageID)

      # Now load and parse new page
      pageJS = getURLText(modifiedURL)

      # Now search this for the video_id
      video_id = re.search(videoIDPat,pageJS)
      if video_id:
        # If found, update database
        video_id = video_id.group(1)
        query = "UPDATE news SET video_id ="+str(video_id)+", comment_prefix='"+domainURL+"' WHERE id="+str(articleID)
        cursor.execute(query)


# Close the db
db.close()
