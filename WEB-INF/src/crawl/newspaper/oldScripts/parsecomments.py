import urllib2
import sys
import re
import os
import urllib2
import json
from include.config import MySQL, FindDate, getURLText

# Connect to DB
mysql = MySQL()
db = mysql.connect()
cursor = db.cursor()
grabcur = db.cursor()
 
grabq = "SELECT news.id,news.video_id,news.comment_prefix FROM news WHERE news.comment_prefix IS NOT NULL and video_id IS NOT NULL"
grabcur.execute(grabq)

# Run the queries on each paper
for link in grabcur.fetchall():
    articleID = str(link[0])
    articleVID = str(link[1])
    articlePrefix = str(link[2])
    
    # Build the JSON link
    jsonLink = articlePrefix + "/refs/" + articleVID + "/smilTexts/comments.json"

    # Load and decode JSON link
    jsonText = urllib2.urlopen(jsonLink).read()

    jsonText = json.loads(jsonText)

    if jsonText['count'] > 0:
      for item in jsonText['items']:
        body = unicode(item['body'])
        insert_data = ( (articleID, body), )
        query = "INSERT INTO news_comments (articleID, body) VALUES (%s, %s)"
        cursor.executemany(query, insert_data)

# Close the db
db.close()
