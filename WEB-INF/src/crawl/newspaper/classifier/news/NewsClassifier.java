package classifier.news;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SMO;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.Utils;
import weka.experiment.InstanceQuery;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NominalToString;
import weka.filters.unsupervised.attribute.StringToWordVector;

class NewsClassifier {

	private String modelDir = "../../Models/";

	private String crimeModel = modelDir+"crimeModel";
	private String protestModel = modelDir+"protestModel";
	private String trafficModel = modelDir+"trafficModel";

	public NewsClassifier() {

	}

	public double testTraffic(String text){
		Instances testData, testcopy;
		FastVector attributes = new FastVector(2);

		FastVector stringVal = null;
		Attribute textAttribute = new Attribute("text", stringVal);
		Attribute classAttribute = new Attribute("subevent", stringVal);
		attributes.addElement(textAttribute);
		attributes.addElement(classAttribute);
		testData = new Instances("TestData", attributes, 1);
		testData.setClassIndex(1);

		Instance inst = new Instance(2);

		inst.setValue(textAttribute, text);
		inst.setMissing(1);

		inst.setDataset(testData);

		testData.add(inst);

		testcopy = new Instances(testData);

		NominalToString filter1 = new NominalToString();

		try {
			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));
			filter1.setInputFormat(testData);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Classifier on training data
		FilteredClassifier fc = null;
		try {
			fc = (FilteredClassifier) SerializationHelper.read(trafficModel);
		} catch (Exception e3) {
			System.out.println("Error reading the model:" + e3.getMessage());
			e3.printStackTrace();
		}

		// Apply filters on test data
		try {
			testData = Filter.useFilter(testData, filter1);
		} catch (Exception e1) {
			System.out.println("Error:" + e1.getMessage());
			e1.printStackTrace();
		}
		testData.setClassIndex(testData.numAttributes() - 1);

		double[] dist = {};
		try {
			dist = fc.distributionForInstance(testData.instance(0));
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			e.printStackTrace();
		}
		return dist[0];

	}
	
	public double testCrime(String text){
		Instances testData, testcopy;
		FastVector attributes = new FastVector(2);

		FastVector stringVal = null;
		Attribute textAttribute = new Attribute("text", stringVal);
		Attribute classAttribute = new Attribute("subevent", stringVal);
		attributes.addElement(textAttribute);
		attributes.addElement(classAttribute);
		testData = new Instances("TestData", attributes, 1);
		testData.setClassIndex(1);

		Instance inst = new Instance(2);

		inst.setValue(textAttribute, text);
		inst.setMissing(1);

		inst.setDataset(testData);

		testData.add(inst);

		testcopy = new Instances(testData);

		NominalToString filter1 = new NominalToString();

		try {
			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));
			filter1.setInputFormat(testData);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Classifier on training data
		FilteredClassifier fc = null;
		try {
			fc = (FilteredClassifier) SerializationHelper.read(crimeModel);
		} catch (Exception e3) {
			System.out.println("Error reading the model:" + e3.getMessage());
			e3.printStackTrace();
		}

		// Apply filters on test data
		try {
			testData = Filter.useFilter(testData, filter1);
		} catch (Exception e1) {
			System.out.println("Error:" + e1.getMessage());
			e1.printStackTrace();
		}
		testData.setClassIndex(testData.numAttributes() - 1);

		double[] dist = {};
		try {
			dist = fc.distributionForInstance(testData.instance(0));
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			e.printStackTrace();
		}
		return dist[0];

	}

	public double testProtest(String text){
		Instances testData, testcopy;
		FastVector attributes = new FastVector(2);

		FastVector stringVal = null;
		Attribute textAttribute = new Attribute("text", stringVal);
		Attribute classAttribute = new Attribute("subevent", stringVal);
		attributes.addElement(textAttribute);
		attributes.addElement(classAttribute);
		testData = new Instances("TestData", attributes, 1);
		testData.setClassIndex(1);

		Instance inst = new Instance(2);

		inst.setValue(textAttribute, text);
		inst.setMissing(1);

		inst.setDataset(testData);

		testData.add(inst);

		testcopy = new Instances(testData);

		NominalToString filter1 = new NominalToString();

		try {
			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));
			filter1.setInputFormat(testData);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Classifier on training data
		FilteredClassifier fc = null;
		try {
			fc = (FilteredClassifier) SerializationHelper.read(protestModel);
		} catch (Exception e3) {
			System.out.println("Error reading the model:" + e3.getMessage());
			e3.printStackTrace();
		}

		// Apply filters on test data
		try {
			testData = Filter.useFilter(testData, filter1);
		} catch (Exception e1) {
			System.out.println("Error:" + e1.getMessage());
			e1.printStackTrace();
		}
		testData.setClassIndex(testData.numAttributes() - 1);

		double[] dist = {};
		try {
			dist = fc.distributionForInstance(testData.instance(0));
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			e.printStackTrace();
		}
		return dist[0];

	}


}
