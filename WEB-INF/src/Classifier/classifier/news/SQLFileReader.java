package classifier.news;

import java.io.BufferedReader;
import java.io.*;

public class SQLFileReader {
	public static void main(String[] a){
		
	}

	public static String readQuery(String filepath) {
		StringBuilder s = new StringBuilder();
		String str;
		try {
			FileReader fr = new FileReader(new java.io.File(filepath));
			BufferedReader br = new BufferedReader(fr);
			
			while ((str = br.readLine()) != null) {
				s.append(str);
			}
			fr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s.toString();
	}
	
}
