package classifier.news;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

import weka.attributeSelection.PrincipalComponents;
import weka.attributeSelection.Ranker;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SMO;
import weka.classifiers.meta.AttributeSelectedClassifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.experiment.InstanceQuery;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NominalToString;
import weka.filters.unsupervised.attribute.StringToWordVector;

class NewsClassifier {

	private InstanceQuery query;

	private static String crimeTrainingSQLFile = "SQL/CrimeTraining.sql";
	private static String protestTrainingSQLFile = "SQL/ProtestTraining.sql";
	private static String trafficTrainingSQLFile = "SQL/TrafficTraining.sql";

	private String crimeModel = "../Models/crimeModel";
	private String protestModel = "../Models/protestModel";
	private String trafficModel = "../Models/trafficModel";

	public static void main(String argv[]) throws Exception {
    
    // Create a NewsClassifier
    NewsClassifier c = new NewsClassifier();

    // Output the models for crime, traffic, protest
    c.trainCrime();
    c.trainTraffic();
    c.trainProtest();


	}

	public Instances getData(String sql) {
		try {
			query = new InstanceQuery();
		} catch (Exception e1) {
			System.out.println("Error:" + e1.getMessage());
			e1.printStackTrace();
		}

		query.setUsername("entitysearch");

		query.setPassword("hello_dssi");
		query.setQuery(sql);

		// Grab training data
		Instances data = null;
		try {
			data = query.retrieveInstances();
			data.setClassIndex(data.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Error while fetching records from database:"
					+ e.getMessage());
			e.printStackTrace();
		}

		return data;

	}

	public ArrayList<Filter> applyFilterToTraining(Instances trainingData) {

		ArrayList<Filter> filters = new ArrayList<Filter>();

		// Filtering
		String[] options = new String[3];
		options[0] = "-R"; // "range"
		options[1] = "first-last"; // first attribute
		options[2] = "-C";

		NominalToString filter1 = new NominalToString();
		StringToWordVector filter2 = new StringToWordVector();
		try {
			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));

			filter1.setInputFormat(trainingData);

			// add Filter1 to the arrray to be returned
			filters.add(filter1);

			Instances filter1output = Filter.useFilter(trainingData, filter1);

			// new instance of filter
			filter2.setOptions(weka.core.Utils
					.splitOptions("-R 1 -W 300 -prune-rate 0 -T -I -N 1 -S -stemmer weka.core.stemmers.NullStemmer -M 1"));

			// inform filter about dataset **AFTER** setting options
			filter2.setInputFormat(filter1output);

			// add 2nd filter to the array to be returned
			filters.add(filter2);

		} catch (Exception e) {
			System.out.println("Error during pre-processing:" + e.getMessage());
			e.printStackTrace();
		}
		// System.out.println(newData);
		return filters;
	}

	public Filter returnFilter(Instances trainingData) {
		// Filtering
		String[] options = new String[3];
		options[0] = "-R"; // "range"
		options[1] = "first-last"; // first attribute
		options[2] = "-C";

		NominalToString filter1 = new NominalToString();
		StringToWordVector filter2 = new StringToWordVector();
		try {
			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));

			filter1.setInputFormat(trainingData);

			Instances filter1output = Filter.useFilter(trainingData, filter1);

			// new instance of filter
			filter2.setOptions(weka.core.Utils
					.splitOptions("-R 1 -W 300 -prune-rate 0 -T -I -N 1 -S -stemmer weka.core.stemmers.NullStemmer -M 1"));

			// inform filter about dataset **AFTER** setting options
			filter2.setInputFormat(filter1output);

		} catch (Exception e) {
			System.out.println("Error during pre-processing:" + e.getMessage());
			e.printStackTrace();
		}
		return filter2;
	}

	public Classifier buildClassifer(Instances data) {
		Logistic trainedClassifier = new Logistic();
		try {
			trainedClassifier.setOptions(weka.core.Utils
					.splitOptions("-R 1.0E-8 -M -1"));

			System.out.println("Classification Done.");

			// Cross Validation
			Evaluation eval = new Evaluation(data);
			eval.crossValidateModel(trainedClassifier, data, 5, new Random(1));

			System.out.println(eval.toSummaryString(true));
		} catch (Exception e) {
			System.out.println("Error during classification:" + e.getMessage());
			e.printStackTrace();
		}

		return trainedClassifier;

	}

	public void trainCrime() {
		// query for training
		String crimeForTraining = SQLFileReader.readQuery(crimeTrainingSQLFile);
		Instances trainingData;
		trainingData = getData(crimeForTraining);

		NominalToString filter1 = new NominalToString();
		StringToWordVector filter2 = new StringToWordVector();
		try {

			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));

			filter1.setInputFormat(trainingData);

			trainingData = Filter.useFilter(trainingData, filter1);

			// new instance of filter
			filter2.setOptions(weka.core.Utils
					.splitOptions("-R 1 -W 800 -prune-rate 0 -T -I -N 1 -S -stopwords stopwordsCrime.txt -stemmer weka.core.stemmers.NullStemmer -M 1"));

			// inform filter about dataset AFTER setting options
			filter2.setInputFormat(trainingData);
		} catch (Exception e) {
			System.out.println("Error during pre-processing:" + e.getMessage());
			e.printStackTrace();
		}
		trainingData.setClassIndex(trainingData.numAttributes() - 1);

		// Classifier on training data
		FilteredClassifier fc = new FilteredClassifier();
		fc.setFilter(filter2);

		// Set up parameters for the model
		try {

			SMO c = new SMO();
			c.setOptions(weka.core.Utils //
					.splitOptions("weka.classifiers.functions.SMO -D -C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"));
			fc.setClassifier(c);
			PrincipalComponents pc = new PrincipalComponents();
			pc.setOptions(weka.core.Utils
					.splitOptions("weka.attributeSelection.PrincipalComponents -R 0.95 -A 5"));
			Ranker r = new Ranker();
			r.setOptions(weka.core.Utils
					.splitOptions("weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N -1"));
			AttributeSelectedClassifier asc = new AttributeSelectedClassifier();
			asc.setClassifier(c);
			asc.setEvaluator(pc);
			asc.setSearch(r);
		} catch (Exception e2) {
			System.out.println("Error while fetching model:" + e2.getMessage());
			e2.printStackTrace();
		}
		
		//Build the model
		try {
			fc.buildClassifier(trainingData);
		} catch (Exception e1) {
			System.out.println("Error while classifying training data:"
					+ e1.getMessage());
			e1.printStackTrace();
		}

		// save model
		try {
			SerializationHelper.write(crimeModel, fc);
		} catch (Exception e) {
			System.out.println("Error while serializing model:"
					+ e.getMessage());
			e.printStackTrace();
		}
	}

	public void writeToDatabase(String query) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:mysql://engr-dssi01.engr.illinois.edu:3306/entitysearch";
		String dbuser = "entitysearch";
		String password = "hello_dssi";
		try {
			con = DriverManager.getConnection(url, dbuser, password);
			st = con.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void trainTraffic() {
		// query for training
		String trafficForTraining = SQLFileReader
				.readQuery(trafficTrainingSQLFile);
		Instances trainingData;
		trainingData = getData(trafficForTraining);

		NominalToString filter1 = new NominalToString();
		StringToWordVector filter2 = new StringToWordVector();
		try {

			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));

			filter1.setInputFormat(trainingData);

			trainingData = Filter.useFilter(trainingData, filter1);

			// new instance of filter
			filter2.setOptions(weka.core.Utils
					.splitOptions("-R 1 -W 800 -prune-rate 0 -T -I -N 1 -S -stopwords stopwordsTraffic.txt -stemmer weka.core.stemmers.NullStemmer -M 1"));

			// inform filter about dataset **AFTER** setting options
			filter2.setInputFormat(trainingData);
		} catch (Exception e) {
			System.out.println("Error during pre-processing:" + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Filters Applied to training data.");
		trainingData.setClassIndex(trainingData.numAttributes() - 1);

		// Classifier on training data
		FilteredClassifier fc = new FilteredClassifier();
		fc.setFilter(filter2);

		// Load model

		try {
			SMO c = new SMO();
			c.setOptions(weka.core.Utils
					.splitOptions("weka.classifiers.functions.SMO -D -C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"));
			fc.setClassifier(c);

			// Cross Validation
			Evaluation eval = new Evaluation(trainingData);
			eval.crossValidateModel(fc, trainingData, 10, new Random(1));
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toClassDetailsString());

		} catch (Exception e2) {
			System.out.println("Error while fetching model:" + e2.getMessage());
			e2.printStackTrace();
		}
		try {
			fc.buildClassifier(trainingData);
		} catch (Exception e1) {
			System.out.println("Error while classifying training data:"
					+ e1.getMessage());
			e1.printStackTrace();
		}

		// save model
		try {
			SerializationHelper.write(trafficModel, fc);
		} catch (Exception e) {
			System.out.println("Error while serializing model:"
					+ e.getMessage());
			e.printStackTrace();
		}
	}

	public void trainProtest() {
		// query for training
		String protestForTraining = SQLFileReader
				.readQuery(protestTrainingSQLFile);
		Instances trainingData;
		trainingData = getData(protestForTraining);
		System.out
				.println("ClassifyProtest: Fetching protest records for training complete.");

		NominalToString filter1 = new NominalToString();
		StringToWordVector filter2 = new StringToWordVector();
		try {

			filter1.setOptions(weka.core.Utils.splitOptions("-C 1"));

			filter1.setInputFormat(trainingData);

			trainingData = Filter.useFilter(trainingData, filter1);

			// new instance of filter
			filter2.setOptions(weka.core.Utils
					.splitOptions("-R 1 -W 800 -prune-rate 0 -T -I -N 1 -S -stopwords stopwordsProtest.txt -stemmer weka.core.stemmers.NullStemmer -M 1"));

			// inform filter about dataset **AFTER** setting options
			filter2.setInputFormat(trainingData);
		} catch (Exception e) {
			System.out.println("Error during pre-processing:" + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Filters Applied to training data.");
		trainingData.setClassIndex(trainingData.numAttributes() - 1);

		// Classifier on training data
		FilteredClassifier fc = new FilteredClassifier();
		fc.setFilter(filter2);

		// Load model
		try {
			SMO c = new SMO();
			c.setOptions(weka.core.Utils
					.splitOptions("weka.classifiers.functions.SMO -D -C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"));
			fc.setClassifier(c);
			// Cross Validation
			Evaluation eval = new Evaluation(trainingData);
			eval.crossValidateModel(fc, trainingData, 10, new Random(1));
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toClassDetailsString());
		} catch (Exception e2) {
			System.out.println("Error while fetching model:" + e2.getMessage());
			e2.printStackTrace();
		}
		
		try {
			fc.buildClassifier(trainingData);
		} catch (Exception e1) {
			System.out.println("Error while classifying training data:"
					+ e1.getMessage());
			e1.printStackTrace();
		}

		// save model
		try {
			SerializationHelper.write(protestModel, fc);
		} catch (Exception e) {
			System.out.println("Error while serializing model:"
					+ e.getMessage());
			e.printStackTrace();
		}
	}

}
