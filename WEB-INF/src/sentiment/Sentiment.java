// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D2945580E4DCB29CCC5021A11CEBE29F9C5A026A2417E6A86A28D8EA245B2005D49615E18404F2D35B401A853DA51A61004F462F3C44000000

package sentiment;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import crawl.SMDocument;


public class Sentiment extends Classifier
{
  public Sentiment()
  {
    containingPackage = "sentiment";
    name = "Sentiment";
  }

  public String getInputType() { return "crawl.SMDocument"; }
  public String getOutputType() { return "discrete"; }


  public FeatureVector classify(Object __example)
  {
    return new FeatureVector(featureValue(__example));
  }

  public Feature featureValue(Object __example)
  {
    String result = discreteValue(__example);
    return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result), (short) allowableValues().length);
  }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof SMDocument))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'Sentiment(SMDocument)' defined on line 16 of SentimentClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    SMDocument smd = (SMDocument) __example;

    return "" + (smd.getSentiment());
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof SMDocument[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'Sentiment(SMDocument)' defined on line 16 of SentimentClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "Sentiment".hashCode(); }
  public boolean equals(Object o) { return o instanceof Sentiment; }
}

