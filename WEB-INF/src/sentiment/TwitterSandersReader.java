package sentiment;

import java.io.FileReader;
import java.io.IOException;

import LBJ2.parse.Parser;

import au.com.bytecode.opencsv.CSVReader;

import crawl.SMDocument;

/**
 * Reads SMDocuments from the Sanders twitter data.
 * 
 * @author Chase Geigle
 */
public class TwitterSandersReader implements Parser {

    private CSVReader fileReader;
    private String fileName;

    public TwitterSandersReader( String fileName ) {
        this.fileName = fileName;
        try {
            initializeReader();
        } catch( IOException e ) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void initializeReader() throws IOException {
        fileReader = new CSVReader( new FileReader( fileName ) );
        fileReader.readNext();
    }
    
    public void close() {
        try {
            fileReader.close();
        } catch( IOException e ) {
            e.printStackTrace();
        }
    }

    public SMDocument next() {
        try {
            String[] line = fileReader.readNext();
            if( line == null )
                return null;
            return new SMDocument()
                .setBody( line[4] )
                .setSentiment( line[1] );
        } catch( IOException e ) {
            e.printStackTrace();
            return null;
        }
    }

    public void reset() {
        try { 
            initializeReader();
        } catch( Exception e ) {
            e.printStackTrace();
        }
    }
}
