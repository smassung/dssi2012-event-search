package sentiment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

import java.util.HashMap;

import LBJ2.classify.ScoreSet;

import crawl.SMDocument;

public class FindSentiment {
    public static void main( String[] args ) throws Exception {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        SentimentClassifier sentimentClassifier = new SentimentClassifier();
        HashMap<String, Integer> sentimentHistogram = new HashMap<String, Integer>();
        while( true ) {
            System.out.print("File: ");
            String file = stdin.readLine();
            if( file.isEmpty() )
                break;
            BufferedReader fileReader = new BufferedReader(new FileReader(file));
            while( fileReader.ready() ) {
                String tweet = fileReader.readLine();
                String sentiLabel = sentimentClassifier.discreteValue(new SMDocument().setBody(tweet));
                ScoreSet scoreSet = sentimentClassifier.scores(new SMDocument().setBody(tweet));
                System.out.println(sentiLabel);
                System.out.println(scoreSet.getScore("positive").toString() + " " + scoreSet.getScore("negative").toString() + " " + scoreSet.getScore("neutral"));
                System.out.println("\t" + tweet);
                if( sentimentHistogram.containsKey( sentiLabel ) )
                    sentimentHistogram.put(sentiLabel, sentimentHistogram.get(sentiLabel) + 1);
                else
                    sentimentHistogram.put(sentiLabel, 1);
            }
            for( String key : sentimentHistogram.keySet() )
                System.out.println( key + ": " + sentimentHistogram.get(key) );
        }
        System.out.println("Exiting...");
    }
}
