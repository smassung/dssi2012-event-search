// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000054D8D3B02C04C068FFA40148B25C2ABAD671FB63727070BD14BD8530DED9C525444DFFE6EC61C924ED7F9C39A98B2F8283583A3F5FE0FC2DB746378DF6C55D7786508BBA328C660F2838872BD0C5C5DF48C34E94382BE61FC675A19982D1983590F0591B2418D48FED2989941507C3158EAEC38125339203F4574630F94B86B19B5A017C41DFFF58E49CF1272A25F0EA062C6901F6BBBB8E7687FBF7DF860D02BC1611C1593B246B7C41E30C869170DD214CF01E7EB09046524320100000

package sentiment;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.nlp.SentenceSplitter;
import LBJ2.nlp.WordSplitter;
import LBJ2.nlp.seg.PlainToTokenParser;
import LBJ2.nlp.seg.Token;
import LBJ2.parse.*;
import LBJ2.parse.ChildrenFromVectors;
import crawl.SMDocument;
import edu.illinois.cs.cogcomp.lbj.pos.POSTagger;
import java.util.ArrayList;


public class WordFeatures extends Classifier
{
  public WordFeatures()
  {
    containingPackage = "sentiment";
    name = "WordFeatures";
  }

  public String getInputType() { return "crawl.SMDocument"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof SMDocument))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'WordFeatures(SMDocument)' defined on line 14 of SentimentClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    SMDocument smd = (SMDocument) __example;

    FeatureVector __result;
    __result = new FeatureVector();
    String __id;
    String __value;

    String body = smd.getCleanBody();
    String[] words = body.split("\\s+");
    for (int i = 0; i < words.length; ++i)
    {
      String word = words[i];
      if (word.isEmpty() || word.length() <= 1)
      {
        continue;
      }
      __id = "" + (word);
      __value = "true";
      __result.addFeature(new DiscretePrimitiveStringFeature(this.containingPackage, this.name, __id, __value, valueIndexOf(__value), (short) 0));
    }
    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof SMDocument[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'WordFeatures(SMDocument)' defined on line 14 of SentimentClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "WordFeatures".hashCode(); }
  public boolean equals(Object o) { return o instanceof WordFeatures; }
}

