package sentiment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

import java.util.HashSet;

import crawl.SMDocument;

import web.SQLConnection;
import web.Tweet;

public class TwitterSentimentSaver {
    public static void main( String [] args ) throws Exception {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Connecting to database...");
        // connect to database
        if( !SQLConnection.connect() ) {
            System.err.println("Failed to connect to database!");
            System.exit(-1);
        }
        // create classifier
        SentimentClassifier sentiClassifier = new SentimentClassifier();
        while( true ) {
            System.out.print("Event name: ");
            String eventId = stdin.readLine();
            if( eventId.isEmpty() )
                break;
            System.out.print("Tweets file: ");
            String tweetsFile = stdin.readLine();
            BufferedReader tweetsReader = new BufferedReader(new FileReader(tweetsFile));
            HashSet<String> bodiesSeen = new HashSet<String>();
            int tweets = 0;
            while( tweetsReader.ready() ) {
                String tweetBody = tweetsReader.readLine();
                if( tweetBody.isEmpty() ) {
                    System.out.println("Skipping empty bodied tweet...");
                    continue;
                }
                if( tweetBody.startsWith("RT") ) {
                    System.out.println("Skipping retweet...");
                    continue;
                }
                if( bodiesSeen.contains(tweetBody) ) {
                    System.out.println("Skipping duplicate tweet...");
                    continue;
                }
                bodiesSeen.add(tweetBody);
                Tweet tweet = new Tweet()
                    .body( tweetBody )
                    .eventId( Integer.parseInt(eventId) )
                    .sentiment( sentiClassifier.discreteValue( new SMDocument().setBody(tweetBody) ) );
                tweet.save();
                if( ++tweets % 100 == 0 )
                    System.out.println("[" + tweets + "] processed...");
            }
        }
        System.out.println("Exiting..");
    }
}
