// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D294555580E4DCB29CCC5021EC93985C5C9996999A54A2A268A11CEBE29F9C5A0217582ECD41D450B1D5580FCF2A417B4D4C292D2A4D26D15800FF06407000804746CFF4000000

package sentiment;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.nlp.SentenceSplitter;
import LBJ2.nlp.WordSplitter;
import LBJ2.nlp.seg.PlainToTokenParser;
import LBJ2.nlp.seg.Token;
import LBJ2.parse.*;
import LBJ2.parse.ChildrenFromVectors;
import crawl.SMDocument;
import edu.illinois.cs.cogcomp.lbj.pos.POSTagger;
import java.util.ArrayList;


public class SentimentClassifier$$1 extends Classifier
{
  private static final WordFeatures __WordFeatures = new WordFeatures();
  private static final POSFeatures __POSFeatures = new POSFeatures();

  public SentimentClassifier$$1()
  {
    containingPackage = "sentiment";
    name = "SentimentClassifier$$1";
  }

  public String getInputType() { return "crawl.SMDocument"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof SMDocument))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'SentimentClassifier$$1(SMDocument)' defined on line 47 of SentimentClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    FeatureVector __result;
    __result = new FeatureVector();
    __result.addFeatures(__WordFeatures.classify(__example));
    __result.addFeatures(__POSFeatures.classify(__example));
    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof SMDocument[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'SentimentClassifier$$1(SMDocument)' defined on line 47 of SentimentClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "SentimentClassifier$$1".hashCode(); }
  public boolean equals(Object o) { return o instanceof SentimentClassifier$$1; }

  public java.util.LinkedList getCompositeChildren()
  {
    java.util.LinkedList result = new java.util.LinkedList();
    result.add(__WordFeatures);
    result.add(__POSFeatures);
    return result;
  }
}

