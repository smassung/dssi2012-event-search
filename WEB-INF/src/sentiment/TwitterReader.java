package sentiment;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import LBJ2.parse.Parser;

import au.com.bytecode.opencsv.CSVReader;

import crawl.SMDocument;

/**
 * Reads SMDocuments from the twitter data.
 * 
 * @author Chase Geigle
 */
public class TwitterReader implements Parser {

    private CSVReader fileReader;
    private String fileName;

    public TwitterReader( String fileName ) {
        this.fileName = fileName;
        try {
            initializeReader();
        } catch( FileNotFoundException e ) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void initializeReader() throws FileNotFoundException {
        fileReader = new CSVReader( new FileReader( fileName ) );
    }
    
    private String sentiment( String num ) {
        if( "0".equals(num) )
            return "negative";
        if( "4".equals(num) )
            return "positive";
        return "neutral";
    }

    public void close() {
        try {
            fileReader.close();
        } catch( IOException e ) {
            e.printStackTrace();
        }
    }

    public SMDocument next() {
        try {
            String[] line = fileReader.readNext();
            if( line == null )
                return null;
            return new SMDocument()
                .setBody( line[5] )
                .setSentiment( sentiment(line[0]) );
        } catch( IOException e ) {
            e.printStackTrace();
            return null;
        }
    }

    public void reset() {
        try { 
            initializeReader();
        } catch( Exception e ) {
            e.printStackTrace();
        }
    }
}
