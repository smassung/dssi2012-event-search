package subtopic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.MySqlTest;

public class ArticleToParagraph {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		runForAllArticles();
	}
	
	public static void runForAllArticles(){
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:mysql://engr-dssi01.engr.illinois.edu:3306/entitysearch";
		String user = "entitysearch";
		String password = "hello_dssi";
		ArrayList<String> id = new ArrayList<String>();
		ArrayList<String> text = new ArrayList<String>();

		try {
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();
			rs = st.executeQuery("SELECT id, text from news_training;");

			while (rs.next()) {
				id.add(rs.getString("id"));
				text.add(rs.getString("text"));
			}
			
			for(int i=0;i<text.size();i++){
				ArrayList<String> para=getParagraphs(100, text.get(i));
				for(int j=0;j<para.size();j++){
					String temp=para.get(j);
					temp=temp.replace("\'","\\\'");
					temp=temp.replace("\"", "\\\"");
//					System.out.println("insert into news_training_paras (article_id,content) values(\""+id.get(i)+"\",\""+temp+"\");");
					st.executeUpdate("insert into news_training_paras (article_id,content) values(\""+id.get(i)+"\",\""+temp+"\");");
//					break;
				}
//				break;
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(MySqlTest.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(MySqlTest.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);
			}
		}
	}

	public static ArrayList<String> getParagraphs(int paraLength, String text) {
		ArrayList<String> para = new ArrayList<String>();

//		String[] tempPara = text.split("\r\n");
		String[] tempPara = text.split("\n");

		for (int i = 0; i < tempPara.length; i++) {
			if (tempPara[i].length() > paraLength) {
				para.add(tempPara[i]);
			}
		}

		return para;
	}

}
