package common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @param T stores the object
 */
public class Pagination<T> implements Serializable{
	private static final long serialVersionUID = -2046803242155647335L;
	
	/** current page */
	private int currentPage;	
	/** total number of pages */
	private int totalPage;
	/** total number of records */
	private long recordSum;
	/** number of records per page*/
	private int countPerPage;
	/** object list */
	private List<T> recordList;
	
	public Pagination(int currentPage, int countPerPage){
		this.currentPage = currentPage;
		this.countPerPage = countPerPage;
	}
	
	public void setRecordSum(long sum) {
		this.recordSum = sum;
		
		//get the total number of pages
		totalPage = (int)(sum/countPerPage);
		if((sum%countPerPage > 0) || totalPage==0){
			totalPage++;
		}		
	}

    /**
     * Calculate the total number of pages based on <code>pageSize</code> (the number of records on each page) 
     * and <code>sum</code> (total number of records).
     * @param sum
     * @param pageSize
     * @return
     */
    public static int totalPages(int sum, int pageSize){
        int myTotalPage = (int)(sum/pageSize);
		if((sum%pageSize > 0) || myTotalPage==0){
			myTotalPage++;
		}	
        return myTotalPage;
    }

	public void setRecordList(List<T> recordList) {		
		this.recordList = recordList;
	}
	
	public List<T> getRecordList(){
		if(recordList==null){
			return new ArrayList<T>();
		}else{
			return this.recordList;
		}
	}

    /**
     * Get the number of records for the current page.
     * @return
     */
    public int getCurrentRealPageSize(){
        return getRecordList().size();
    }
	
	public void addRecord(T record){
		if(recordList==null){
			recordList = new ArrayList<T>();
		}
		
		recordList.add(record);
	}
	
	public int getCountPerPage() {
		return countPerPage;
	}
	
	public int getCurrentPage() {
		return currentPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public long getRecordSum(){
		return this.recordSum;
	}

	public int getNextPage(){
		if(currentPage >= totalPage){
			return totalPage;
		}else{
			return (currentPage+1);
		}
	}
	
	public int getPreviousPage(){		
		if(currentPage <= 1){
			return 1;
		}else{
			return (currentPage-1);
		}		
	}
	
	/**
	 * Get the page numbers before and after the current page. For example, if the current page number is 5, the final result would be
	 * 1,2,3,4,5,6,7,8,9,10
	 * 
	 * @return
	 */
	public List<Integer> getPages(){
		LinkedList<Integer> pages = new LinkedList<Integer>();
		pages.add(getCurrentPage());
		int gap = 1;
		while(pages.size()<10 && ((currentPage-gap)>0 ||(currentPage+gap)<=totalPage)){
			if((currentPage-gap)>0){
				pages.addFirst(currentPage-gap);
			}
			
			if((currentPage + gap)<=totalPage){
				pages.addLast(currentPage+gap);
			}
			
			gap++;
		}
		
		return pages;
	}
	
    @Override
	public String toString(){
		StringBuilder sb =  new StringBuilder();
		sb.append("recordSum=" + this.recordSum);
		sb.append("; totalPage=" + this.getTotalPage());
		sb.append("\n_________________________\n");
		
		for(T t: getRecordList()){
			sb.append(t);
			sb.append("\n");
		}
		return sb.toString();
	}
	
	/**
	 * Return the position of the first record on the current page.
	 * @return
	 */
	public int getPosition(){
		return (currentPage-1)*countPerPage + 1;
	}
}
