package retrieval;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;

import org.apache.lucene.analysis.en.EnglishAnalyzer;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.NumericField;

import org.apache.lucene.index.IndexReader;

import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;

import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;

import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TokenSources;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.apache.lucene.util.Version;

import web.Tweet;

/**
 * Static retriever class that performs searches against the index build
 * by the Indexer tool.
 * 
 * @author Chase Geigle
 */
public class Retriever {
    private static Directory directory;
    private static Analyzer analyzer;
    private static IndexReader reader;
    private static IndexSearcher searcher;
    private static QueryParser parser;
    private static SimpleHTMLFormatter formatter;

    public static void connect() throws IOException {
        connect("/data/lucene/index");
    }

    public static void connect( String location ) throws IOException {
        System.out.println( location );
        analyzer = new EnglishAnalyzer( Version.LUCENE_36 );
        directory = FSDirectory.open( new File(location) );
        reader = IndexReader.open( directory );
        searcher = new IndexSearcher( reader );
        parser = new QueryParser( Version.LUCENE_36, "searchText", analyzer );
        formatter = new SimpleHTMLFormatter();
    }

    public static List<Document> search( String query ) throws ParseException, IOException {
        ArrayList<Document> docs = new ArrayList<Document>();
        Query lucQuery = parser.parse( QueryParser.escape(query) );
        ScoreDoc[] hits = searcher.search( lucQuery, null, 1000 ).scoreDocs;
        for( ScoreDoc scoreDoc : hits )
            docs.add( searcher.doc( scoreDoc.doc ) );
        return docs;
    }

    public static List<Document> searchBySubEvent( String query, String subEvent ) throws ParseException, IOException {
        ArrayList<Document> docs = new ArrayList<Document>();
        String realQuery = "searchText:" + QueryParser.escape(query) + " AND subEvent:" + QueryParser.escape(subEvent);
        Query lucQuery = parser.parse( realQuery );
        ScoreDoc[] hits = searcher.search( lucQuery, null, 1000 ).scoreDocs;
        for( ScoreDoc scoreDoc : hits ) {
            Document doc = searcher.doc( scoreDoc.doc );
            doc.add( new NumericField("docid").setIntValue( scoreDoc.doc ) );
            docs.add( doc );
        }
        return docs;
    }

    public static int getDocId( Document document ) {
        return ( (NumericField)document.getFieldable("docid") ).getNumericValue().intValue();
    }

    public static Explanation explain( String query, Document doc ) throws ParseException, IOException {
        return searcher.explain( parser.parse(query), getDocId(doc) );
    }

    public static String getSnippet( Document document, String query ) throws ParseException, IOException, InvalidTokenOffsetsException {
        return getSnippet( document, query, 6 );
    }

    public static String getSnippet( Document document, String query, int numFragments ) throws ParseException, IOException, InvalidTokenOffsetsException {
        Query lucQuery = parser.parse( QueryParser.escape(query) );
        Highlighter highlighter = new Highlighter( formatter, new QueryScorer(lucQuery) );
        TokenStream tokenStream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), getDocId(document), "fullText", analyzer);
        return highlighter.getBestFragments( tokenStream, document.get("fullText"), numFragments, "..." );
    }

    public static float getSentiment( Document document ) {
        return ( (NumericField)document.getFieldable("sentiment") ).getNumericValue().floatValue();
    }

    public static float getPositiveSentiment( Document document ) {
        return ( (NumericField)document.getFieldable("positive") ).getNumericValue().floatValue();
    }

    public static List<Tweet> getTweets( Document document ) {
        return Tweet.findByEventId( getEventId(document) );
    }

    public static int getEventId( Document document ) {
        return ( (NumericField)document.getFieldable("eventId") ).getNumericValue().intValue();
    }

    public static void disconnect() throws IOException {
        searcher.close();
        reader.close();
        directory.close();
    }

    /**
     * Main method for testing. =)
     */
    public static void main( String[] args ) throws Exception {
        System.out.println( "Connecting to lucene..." );
        if( args.length == 0 )
            connect();
        else
            connect(args[0]);
        BufferedReader stdin = new BufferedReader( new InputStreamReader( System.in ) );
        while( true ) {
            System.out.print("Query: ");
            String query = stdin.readLine();
            if( query.isEmpty() )
                break;
            System.out.print("Subevent: ");
            String subevent = stdin.readLine();
            List<Document> docs = searchBySubEvent( query, subevent );
            for( int i = 0; i < docs.size(); ++i ) {
                Document doc = docs.get(i);
                System.out.println( doc.get("title") + ": " + doc.get("url") );
                System.out.println( "event:\t\t" + doc.get("event") );
                System.out.println( "subevent:\t" + doc.get("subEvent") );
                System.out.println( "sentiment:\t" + ((NumericField)doc.getFieldable("sentiment")).getNumericValue().floatValue() );
                System.out.println( "==== Snippet ====" );
                System.out.println( getSnippet(doc, query) );
                System.out.println( "==== Snippet ====" );
                System.out.println( explain( query, doc ) );
                String line = stdin.readLine();
                if( "q".equals(line) )
                    break;
            }
            System.out.println("====================");
        }
        disconnect();
    }
}
