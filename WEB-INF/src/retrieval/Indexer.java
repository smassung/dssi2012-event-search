package retrieval;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;

import org.apache.lucene.analysis.en.EnglishAnalyzer;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.apache.lucene.util.Version;

import web.NewsArticle;
import web.SQLConnection;

/**
 * Indexes documents into Lucene from crawled data. By default, it will
 * wipe the entire index and start from scratch (since in general that's
 * probaby what we want to do anyway if we are playing with sentiment
 * boosts and whatnot).
 * 
 * @author Chase Geigle
 */
public class Indexer {
    private static NumericField idfield = new NumericField("id", Field.Store.YES, false);
    private static NumericField sentifield = new NumericField("sentiment", Field.Store.YES, false);
    private static NumericField eventidfield = new NumericField("eventId", Field.Store.YES, false);
    private static NumericField positivefield = new NumericField("positive", Field.Store.YES, false);
    public static void main( String[] args ) {
        try {
            if( establishedConnection() ) {
                if( args.length == 0 )
                    runIndexer("/data/lucene/index");
                else
                    runIndexer(args[0]);
                System.out.println( "Finished indexing!" );
            }
        } catch( Exception e ) {
            e.printStackTrace();
            System.err.println( "Something horrible happened!" );
        }
    }

    public static boolean establishedConnection() {
        System.out.println( "Establishing database connection... " );
        boolean ret = SQLConnection.connect();
        if( !ret )
            System.err.println( "Failed to connect to database!" );
        return ret;
    }

    public static void runIndexer( String location ) throws IOException {
        Analyzer analyzer = new EnglishAnalyzer( Version.LUCENE_36 );

        Directory directory = FSDirectory.open( new File(location) );

        IndexWriterConfig iwriterConfig = new IndexWriterConfig( Version.LUCENE_36, analyzer );
        iwriterConfig.setOpenMode( IndexWriterConfig.OpenMode.CREATE );

        IndexWriter iwriter = new IndexWriter( directory, iwriterConfig );

        // write all articles to the index
        int numWritten = 0;
        for( NewsArticle article : NewsArticle.all() ) {
            iwriter.addDocument( createDocument( article ) );
            if( ++numWritten % 100 == 0 )
                System.out.println( numWritten + " documents processed..." );
        }
        iwriter.close();
        directory.close();
    }

    public static Document createDocument( NewsArticle article ) {
        Document doc = new Document();
        // store article id
        doc.add( idfield.setIntValue( article.id() ) );
        // index the analyzed text for searching
        doc.add( 
            new Field( 
                "searchText",
                article.text(),
                Field.Store.YES,
                Field.Index.ANALYZED
            )
        );
        // index the full text for snippet generation
        doc.add(
            new Field(
                "fullText",
                article.text(),
                Field.Store.YES,
                Field.Index.NOT_ANALYZED
            )
        );
        doc.add(
            new Field(
                "title",
                article.title(),
                Field.Store.YES,
                Field.Index.NOT_ANALYZED
            )
        );
        String date = "unknown";
        if( article.date() != null )
            date = article.date().toString();
        doc.add(
            new Field(
                "date",
                date,
                Field.Store.YES,
                Field.Index.NOT_ANALYZED
            )
        );
        doc.add(
            new Field(
                "location",
                article.location(),
                Field.Store.YES,
                Field.Index.NOT_ANALYZED
            )
        );
        doc.add(
            new Field(
                "url",
                article.url(),
                Field.Store.YES,
                Field.Index.NOT_ANALYZED
            )
        );
        doc.add(
            new Field(
                "event",
                article.event(),
                Field.Store.YES,
                Field.Index.NOT_ANALYZED
            )
        );
        doc.add( eventidfield.setIntValue( article.eventId() ) );
        for( String subEvent : article.subEvents() ) {
            doc.add(
                new Field(
                    "subEvent",
                    subEvent,
                    Field.Store.YES,
                    Field.Index.NOT_ANALYZED
                )
            );
        }
        float senti = article.sentiment();
        doc.add( sentifield.setFloatValue(senti) );
        doc.setBoost( senti );
        float positive = article.positiveSentiment();
        doc.add( positivefield.setFloatValue(positive) );
        return doc;
    }
}
