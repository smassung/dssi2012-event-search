require 'csv'
file = CSV.read('full-corpus.csv')
entries = file[1..-1]
entries.shuffle!

# grab equal number of neutral, positive, negative for training
positive = entries.find_all{|e| e[1] == "positive"}
negative = entries.find_all{|e| e[1] == "negative"}
neutral = entries.find_all{|e| e[1] == "neutral"}
train = CSV.open('train.csv', 'w')
test = CSV.open('test.csv', 'w')

positives = positive.each_slice(ARGV[0].to_i).to_a
negatives = negative.each_slice(ARGV[0].to_i).to_a
neutrals = neutral.each_slice(ARGV[0].to_i).to_a


remaining = [
  positives[1..-1].flatten(1).length,
  negatives[1..-1].flatten(1).length,
  neutrals[1..-1].flatten(1).length
].min

positives[0].each do |p|
  train << p
end

pp = positives[1..-1].flatten(1)
for i in (0..remaining-1) do
  test << pp[i]
end

negatives[0].each do |n|
  train << n
end

nn = negatives[1..-1].flatten(1)
for i in (0..remaining-1) do
  test << nn[i]
end

neutrals[0].each do |n|
  train << n
end

nn = neutrals[1..-1].flatten(1)
for i in (0..remaining-1) do
  test << nn[i]
end
