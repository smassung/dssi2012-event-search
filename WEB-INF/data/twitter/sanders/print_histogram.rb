require 'csv'
["test.csv", "train.csv"].each do |csvfile|
  h = {}
  CSV.open(csvfile).each do |row|
    h[row[1]] = h[row[1]].to_i + 1
  end
  puts "#{csvfile}: #{h}"
end
