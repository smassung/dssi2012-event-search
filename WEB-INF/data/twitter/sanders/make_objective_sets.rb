require 'csv'

entries = CSV.read('full-corpus.csv')
entries.shuffle!

subjective = entries.find_all{|e| e[1] == "positive" || e[1] == "negative"}
objective = entries.find_all{|e| e[1] == "neutral"}

min = [subjective.length, objective.length].min

train = CSV.open("train.csv", 'w')
test = CSV.open("test.csv", 'w')

for i in (0..ARGV[0].to_i-1)
  train << subjective[i]
  train << objective[i]
end

for i in (ARGV[0].to_i..min-1)
  test << subjective[i]
  test << objective[i]
end
