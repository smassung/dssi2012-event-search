#!/bin/bash

if [ $# -eq 0 ]; 
then
    echo "Usage: compile <lbj file>"
fi

FILE=$*


JAVA=java

JAVAC=javac

JAVA="nice "$JAVA
SWITCHES="-ea -XX:MaxPermSize=1g -Xmx8g"

BIN=classes
LBJBIN=classes
SRC=src
GSP=$SRC

CP=$BIN:$LBJBIN:lib/*:../JARs/*

$JAVA $SWITCHES -cp $CP LBJ2.Main -d $LBJBIN -gsp $GSP -sourcepath $SRC $FILE
